/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Handler;

import Entities.Match;
import Entities.Tournoi;
import java.util.Date;
import java.util.Vector;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 *
 * @author ferid
 */
public class TournoiHandler  extends DefaultHandler{
      private Vector TournoiVector;
      
      public TournoiHandler () {
          TournoiVector =new Vector ();
      }
      
       public Tournoi[] getTournoi() {
        Tournoi[] personTab = new Tournoi[TournoiVector.size()];
        TournoiVector.copyInto(personTab);
        return personTab;
    }
        String selectedBalise = "";
    Tournoi seclectedTournoi;
    
        public void startElement(String string, String string1, String qName, Attributes atrbts) throws SAXException {
        if (qName.equals("tournoi")) {
            seclectedTournoi = new Tournoi();
        } else if (qName.equals("id_event")) {
            selectedBalise = "id_event";
        } else if (qName.equals("nom")) {
            selectedBalise = "nom";
        } else if (qName.equals("emplacement")) {
            selectedBalise = "emplacement";
        }  else if (qName.equals("date_ouverture")) {
            selectedBalise = "date_ouverture";
        } else if (qName.equals("date_cloture")) {
            selectedBalise = "date_cloture";
        } else if (qName.equals("type_event")) {
            selectedBalise = "type_event";
        } else if (qName.equals("type_tournoi")) {
            selectedBalise = "type_tournoi";
        } else if (qName.equals("logo")) {
            selectedBalise = "logo";
        } 
        
        
    }
        
            public void endElement(String string, String string1, String qName) throws SAXException {
        if (qName.equals("tournoi")) {

            TournoiVector.addElement(seclectedTournoi);
            seclectedTournoi = null;
        }else if (qName.equals("id_event")) {
            selectedBalise = "";
        } else if (qName.equals("nom")) {
            selectedBalise = "";
        } else if (qName.equals("emplacement")) {
            selectedBalise = "";
        }  else if (qName.equals("date_ouverture")) {
            selectedBalise = "";
        } else if (qName.equals("date_cloture")) {
            selectedBalise = "";
        } else if (qName.equals("type_event")) {
            selectedBalise = "";
        } else if (qName.equals("type_tournoi")) {
            selectedBalise = "";
        } else if (qName.equals("logo")) {
            selectedBalise = "";
        } 
        
        
        
    }
                public void characters(char[] chars, int i, int i1) throws SAXException {
        if (seclectedTournoi != null) {
            if (selectedBalise.equals("id_event")) {
                seclectedTournoi.setId_event(Integer.parseInt(new String(chars, i, i1)));
                System.out.println(new String(chars, i, i1));
            }
            if (selectedBalise.equals("nom")) {
                seclectedTournoi.setNom(new String(chars, i, i1));
                System.out.println(new String(chars, i, i1));
            }
            if (selectedBalise.equals("emplacement")) {
                seclectedTournoi.setEmplacement(new String(chars, i, i1));
                System.out.println(new String(chars, i, i1));
            }
            
             if (selectedBalise.equals("date_ouverture")) {
                seclectedTournoi.setDate_ouverture(new String(chars, i, i1));
                System.out.println(new String(chars, i, i1));
            }
              if (selectedBalise.equals("date_cloture")) {
                seclectedTournoi.setDate_cloture(new String(chars, i, i1));
                System.out.println(new String(chars, i, i1));
              }
                if (selectedBalise.equals("type_event")) {
                seclectedTournoi.setType_event(new String(chars, i, i1));
                System.out.println(new String(chars, i, i1));
            }
                 if (selectedBalise.equals("type_tournoi")) {
                seclectedTournoi.setType_tournoi(new String(chars, i, i1));
                System.out.println(new String(chars, i, i1));
            }
                  if (selectedBalise.equals("logo")) {
                seclectedTournoi.setLogo(new String(chars, i, i1));
                System.out.println(new String(chars, i, i1));
            }
                
            
             
              
              
              
        }
    }
    
}
