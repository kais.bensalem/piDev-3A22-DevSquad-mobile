/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Handler;

import Entities.Medecin;
import java.util.Vector;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 *
 * @author ferid
 */
public class MedecinHandler  extends DefaultHandler{
      private Vector medecinVector;
      
      public MedecinHandler () {
          medecinVector =new Vector ();
      }
      
       public Medecin[] getMedecin() {
        Medecin[] personTab = new Medecin[medecinVector.size()];
        medecinVector.copyInto(personTab);
        return personTab;
    }
        String selectedBalise = "";
    Medecin seclectedMatch;
    
        public void startElement(String string, String string1, String qName, Attributes atrbts) throws SAXException {
        if (qName.equals("medecin")) {
            seclectedMatch = new Medecin();
        } else if (qName.equals("id")) {
            selectedBalise = "id";
        } else if (qName.equals("nom")) {
            selectedBalise = "nom";
        } else if (qName.equals("prenom")) {
            selectedBalise = "prenom";
        } else if (qName.equals("sexe")) {
            selectedBalise = "sexe";
        } else if (qName.equals("date_naissance")) {
            selectedBalise = "date_naissance";
        } else if (qName.equals("adresse")) {
            selectedBalise = "adresse";
        } else if (qName.equals("email")) {
            selectedBalise = "email";
        } else if (qName.equals("nationalite")) {
            selectedBalise = "nationalite";
        }
        
    }
        
            public void endElement(String string, String string1, String qName) throws SAXException {
        if (qName.equals("medecin")) {

            medecinVector.addElement(seclectedMatch);
            seclectedMatch = null;
        } else if (qName.equals("id")) {
            selectedBalise = "id";
        } else if (qName.equals("nom")) {
            selectedBalise = "nom";
        } else if (qName.equals("prenom")) {
            selectedBalise = "prenom";
        } else if (qName.equals("sexe")) {
            selectedBalise = "sexe";
        } else if (qName.equals("date_naissance")) {
            selectedBalise = "date_naissance";
        } else if (qName.equals("adresse")) {
            selectedBalise = "adresse";
        } else if (qName.equals("email")) {
            selectedBalise = "email";
        } else if (qName.equals("nationalite")) {
            selectedBalise = "nationalite";
        }
        
    }
                public void characters(char[] chars, int i, int i1) throws SAXException {
        if (seclectedMatch != null) {
            if (selectedBalise.equals("id")) {
                seclectedMatch.setId(Integer.parseInt(new String(chars, i, i1)));
                System.out.println(new String(chars, i, i1));
            }
            if (selectedBalise.equals("nom")) {
                seclectedMatch.setNom(new String(chars, i, i1));
                System.out.println(new String(chars, i, i1));
            }
            if (selectedBalise.equals("prenom")) {
                seclectedMatch.setPrenom(new String(chars, i, i1));
                System.out.println(new String(chars, i, i1));
            }
            if (selectedBalise.equals("sexe")) {
                seclectedMatch.setSexe(new String(chars, i, i1));
                System.out.println(new String(chars, i, i1));
            }
            
             if (selectedBalise.equals("date_naissance")) {
                seclectedMatch.setDate_naissance(new String(chars, i, i1));
                System.out.println(new String(chars, i, i1));
            }
             
              if (selectedBalise.equals("adresse")) {
                seclectedMatch.setAdresse(new String(chars, i, i1));
                System.out.println(new String(chars, i, i1));
            }
              if (selectedBalise.equals("email")) {
                seclectedMatch.setEmail(new String(chars, i, i1));
                System.out.println(new String(chars, i, i1));
            }
        }     
        if (selectedBalise.equals("nationalite")) {
               seclectedMatch.setNationalite(new String(chars, i, i1));
                System.out.println(new String(chars, i, i1));
            }
    }
    
}
