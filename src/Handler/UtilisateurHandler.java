/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Handler;

import Entities.Utilisateur;
import java.util.Vector;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 *
 * @author ferid
 */
public class UtilisateurHandler  extends DefaultHandler{
      private Vector utilisateurVector;
      
      public UtilisateurHandler () {
          utilisateurVector =new Vector ();
      }
      
       public Utilisateur[] getUtilisateur() {
        Utilisateur[] personTab = new Utilisateur[utilisateurVector.size()];
        utilisateurVector.copyInto(personTab);
        return personTab;
    }
        String selectedBalise = "";
    Utilisateur seclectedMatch;
    
        public void startElement(String string, String string1, String qName, Attributes atrbts) throws SAXException {
        if (qName.equals("utilisateur")) {
            seclectedMatch = new Utilisateur();
        } else if (qName.equals("id")) {
            selectedBalise = "id";
        } else if (qName.equals("nom")) {
            selectedBalise = "nom";
        } else if (qName.equals("prenom")) {
            selectedBalise = "prenom";
        } else if (qName.equals("login")) {
            selectedBalise = "login";
        } else if (qName.equals("password")) {
            selectedBalise = "password";
        } else if (qName.equals("type")) {
            selectedBalise = "type";
        } else if (qName.equals("statut")) {
            selectedBalise = "statut";
        } else if (qName.equals("date_naissance")) {
            selectedBalise = "date_naissance";
        } else if (qName.equals("email")) {
            selectedBalise = "email";
        }
        
    }
        
            public void endElement(String string, String string1, String qName) throws SAXException {
        if (qName.equals("utilisateur")) {

            utilisateurVector.addElement(seclectedMatch);
            seclectedMatch = null;
        } else if (qName.equals("id")) {
            selectedBalise = "id";
        } else if (qName.equals("nom")) {
            selectedBalise = "nom";
        } else if (qName.equals("prenom")) {
            selectedBalise = "prenom";
        } else if (qName.equals("login")) {
            selectedBalise = "login";
        } else if (qName.equals("password")) {
            selectedBalise = "password";
        } else if (qName.equals("type")) {
            selectedBalise = "type";
        } else if (qName.equals("statut")) {
            selectedBalise = "statut";
        } else if (qName.equals("date_naissance")) {
            selectedBalise = "date_naissance";
        } else if (qName.equals("email")) {
            selectedBalise = "email";
        }
        
    }
                public void characters(char[] chars, int i, int i1) throws SAXException {
        if (seclectedMatch != null) {
            if (selectedBalise.equals("id")) {
                seclectedMatch.setId(Integer.parseInt(new String(chars, i, i1)));
                System.out.println(new String(chars, i, i1));
            }
            if (selectedBalise.equals("nom")) {
                seclectedMatch.setNom(new String(chars, i, i1));
                System.out.println(new String(chars, i, i1));
            }
            if (selectedBalise.equals("prenom")) {
                seclectedMatch.setPrenom(new String(chars, i, i1));
                System.out.println(new String(chars, i, i1));
            }
            if (selectedBalise.equals("login")) {
                seclectedMatch.setLogin(new String(chars, i, i1));
                System.out.println(new String(chars, i, i1));
            }
            
             if (selectedBalise.equals("password")) {
                seclectedMatch.setPassword(new String(chars, i, i1));
                System.out.println(new String(chars, i, i1));
            }
             
              if (selectedBalise.equals("type")) {
                seclectedMatch.setType(new String(chars, i, i1));
                System.out.println(new String(chars, i, i1));
            }
              if (selectedBalise.equals("statut")) {
                seclectedMatch.setStatut(Integer.parseInt(new String(chars, i, i1)));
                System.out.println(new String(chars, i, i1));
            }
              if (selectedBalise.equals("date_naissance")) {
                seclectedMatch.setDate_naissance(new String(chars, i, i1));
                System.out.println(new String(chars, i, i1));
            }
              if (selectedBalise.equals("email")) {
                seclectedMatch.setEmail(new String(chars, i, i1));
                System.out.println(new String(chars, i, i1));
            }
        }
    }
    
}
