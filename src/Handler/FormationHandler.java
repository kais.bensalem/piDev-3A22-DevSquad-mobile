/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Handler;

import Entities.Formation;
import java.util.Vector;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 *
 * @author Salamandar
 */
public class FormationHandler extends DefaultHandler {

    private Vector formationVector;

    public FormationHandler() {
        formationVector = new Vector();
    }

    public Formation[] getFormation() {
        Formation[] formationTab = new Formation[formationVector.size()];
        formationVector.copyInto(formationTab);
        return formationTab;
    }
    String selectedBalise = "";
    Formation seclectedFormation;

    public void startElement(String string, String string1, String qName, Attributes atrbts) throws SAXException {
        if (qName.equals("Formation")) {
            seclectedFormation = new Formation();
        } else if (qName.equals("id_event")) {
            selectedBalise = "id_event";
        } else if (qName.equals("nom")) {
            selectedBalise = "nom";
        } else if (qName.equals("date_ouverture")) {
            selectedBalise = "date_ouverture";
        } else if (qName.equals("date_cloture")) {
            selectedBalise = "date_cloture";
        } else if (qName.equals("emplacement")) {
            selectedBalise = "emplacement";
        } else if (qName.equals("nb_participant")) {
            selectedBalise = "nb_participant";
        }

    }

    public void endElement(String string, String string1, String qName) throws SAXException {
        if (qName.equals("Formation")) {

            formationVector.addElement(seclectedFormation);
            seclectedFormation = null;
        } else if (qName.equals("id_event")) {
            selectedBalise = "id_event";
        } else if (qName.equals("nom")) {
            selectedBalise = "nom";
        } else if (qName.equals("date_ouverture")) {
            selectedBalise = "date_ouverture";
        } else if (qName.equals("date_cloture")) {
            selectedBalise = "date_cloture";
        } else if (qName.equals("emplacement")) {
            selectedBalise = "emplacement";
        } else if (qName.equals("nb_participant")) {
            selectedBalise = "nb_participant";
        }
    }
    public void characters(char[] chars, int i, int i1) throws SAXException {
        if (seclectedFormation != null) {
            if (selectedBalise.equals("id_event")) {
                seclectedFormation.setId(Integer.parseInt(new String(chars, i, i1)));
                System.out.println(new String(chars, i, i1));
            }
            if (selectedBalise.equals("nom")) {
                seclectedFormation.setNom(new String(chars, i, i1));
                System.out.println(new String(chars, i, i1));
            }
            if (selectedBalise.equals("date_ouverture")) {
                seclectedFormation.setDate_ouverture(new String(chars, i, i1));
                System.out.println(new String(chars, i, i1));
            }
            
             if (selectedBalise.equals("date_cloture")) {
                seclectedFormation.setDate_cloture(new String(chars, i, i1));
                System.out.println(new String(chars, i, i1));
            }
             
              if (selectedBalise.equals("emplacement")) {
                seclectedFormation.setEmplacement(new String(chars, i, i1));
                System.out.println(new String(chars, i, i1));
            }
              
                if (selectedBalise.equals("nb_participant")) {
                seclectedFormation.setNb_participant(Integer.parseInt(new String(chars, i, i1)));
                System.out.println(new String(chars, i, i1));
            }
                 
             
            
             
              
              
              
        }
    }
    

}
