/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Handler;

import Entities.Match;
import Entities.Reservation;
import Entities.Utilisateur;
import java.util.Vector;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 *
 * @author Raed
 */
public class ReservationHandler extends DefaultHandler {

    private Vector reservations;

    public ReservationHandler() {
        reservations = new Vector();
    }

    public Reservation[] getReservations() {
        Reservation[] newsTab = new Reservation[reservations.size()];
        reservations.copyInto(newsTab);
        return newsTab;
    }
    String selectedBalise = "";
    Reservation seclectedReservation;

    public void startElement(String string, String string1, String qName, Attributes atrbts) throws SAXException {
        if (qName.equals("reservation")) {
            seclectedReservation = new Reservation();
        } else if (qName.equals("id")) {
            selectedBalise = "id";
        } else if (qName.equals("id_user")) {
            selectedBalise = "id_user";
        } else if (qName.equals("id_match")) {
            selectedBalise = "id_match";
        } else if (qName.equals("achat")) {
            selectedBalise = "achat";
        } else if (qName.equals("cat1")) {
            selectedBalise = "cat1";
        } else if (qName.equals("cat2")) {
            selectedBalise = "cat2";
        } else if (qName.equals("cat3")) {
            selectedBalise = "cat3";
        } else if (qName.equals("cat4")) {
            selectedBalise = "cat4";
        } else if (qName.equals("prix")) {
            selectedBalise = "prix";
        }
    }

    public void endElement(String string, String string1, String qName) throws SAXException {
        if (qName.equals("reservation")) {
            reservations.addElement(seclectedReservation);
            System.out.println(seclectedReservation);
            seclectedReservation = null;
        } else if (qName.equals("id")) {
            selectedBalise = "";
        } else if (qName.equals("id_user")) {
            selectedBalise = "";
        } else if (qName.equals("id_match")) {
            selectedBalise = "";
        } else if (qName.equals("achat")) {
            selectedBalise = "";
        } else if (qName.equals("cat1")) {
            selectedBalise = "";
        } else if (qName.equals("cat2")) {
            selectedBalise = "";
        } else if (qName.equals("cat3")) {
            selectedBalise = "";
        } else if (qName.equals("cat4")) {
            selectedBalise = "";
        } else if (qName.equals("prix")) {
            selectedBalise = "";
        }

    }

    public void characters(char[] chars, int i, int i1) throws SAXException {
        if (seclectedReservation != null) {
            if (selectedBalise.equals("id")) {
                seclectedReservation.setId(Integer.parseInt(new String(chars, i, i1)));
            }
            if (selectedBalise.equals("id_user")) {
                seclectedReservation.setU(new Utilisateur(Integer.parseInt(new String(chars, i, i1)), null, null, null, null));
            }
            if (selectedBalise.equals("id_match")) {
                seclectedReservation.setM(new Match(Integer.parseInt(new String(chars, i, i1)), null, null, null, null, 1));
            }
            if (selectedBalise.equals("achat")) {
                seclectedReservation.setAchat(false);
            }
            if (selectedBalise.equals("cat1")) {
                seclectedReservation.setCat1(Integer.parseInt(new String(chars, i, i1)));
            }
            if (selectedBalise.equals("cat2")) {
                seclectedReservation.setCat2(Integer.parseInt(new String(chars, i, i1)));
            }
            if (selectedBalise.equals("cat3")) {
                seclectedReservation.setCat3(Integer.parseInt(new String(chars, i, i1)));
            }
            if (selectedBalise.equals("cat4")) {
                seclectedReservation.setCat4(Integer.parseInt(new String(chars, i, i1)));
            }
            if (selectedBalise.equals("prix")) {
                seclectedReservation.setPrix(Integer.parseInt(new String(chars, i, i1)));
            }

        }
    }
}
