/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Handler;

import Entities.Match;
import java.util.Date;
import java.util.Vector;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 *
 * @author ferid
 */
public class MatchHandler  extends DefaultHandler{
      private Vector MatchVector;
      
      public MatchHandler () {
          MatchVector =new Vector ();
      }
      
       public Match[] getMatch() {
        Match[] personTab = new Match[MatchVector.size()];
        MatchVector.copyInto(personTab);
        return personTab;
    }
        String selectedBalise = "";
    Match seclectedMatch;
    
        public void startElement(String string, String string1, String qName, Attributes atrbts) throws SAXException {
        if (qName.equals("match")) {
            seclectedMatch = new Match();
        } else if (qName.equals("id_match")) {
            selectedBalise = "id_match";
        } else if (qName.equals("nom")) {
            selectedBalise = "nom";
        } else if (qName.equals("prenom")) {
            selectedBalise = "prenom";
        }  else if (qName.equals("date")) {
            selectedBalise = "date";
        } else if (qName.equals("type")) {
            selectedBalise = "type";
        } else if (qName.equals("id_tournoi")) {
            selectedBalise = "id_tournoi";
        } 
        
        
    }
        
            public void endElement(String string, String string1, String qName) throws SAXException {
        if (qName.equals("match")) {

            MatchVector.addElement(seclectedMatch);
            seclectedMatch = null;
        }else if (qName.equals("id_match")) {
            selectedBalise = "id_match";
        } else if (qName.equals("nom")) {
            selectedBalise = "nom";
        } else if (qName.equals("prenom")) {
            selectedBalise = "prenom";
        }  else if (qName.equals("date")) {
            selectedBalise = "date";
        } else if (qName.equals("type")) {
            selectedBalise = "type";
        } else if (qName.equals("id_tournoi")) {
            selectedBalise = "id_tournoi";
        } 
        
        
        
    }
                public void characters(char[] chars, int i, int i1) throws SAXException {
        if (seclectedMatch != null) {
            if (selectedBalise.equals("id")) {
                seclectedMatch.setId_match(Integer.parseInt(new String(chars, i, i1)));
                System.out.println(new String(chars, i, i1));
            }
            if (selectedBalise.equals("date")) {
                seclectedMatch.setDate(new String(chars, i, i1));
                System.out.println(new String(chars, i, i1));
            }
            if (selectedBalise.equals("type")) {
                seclectedMatch.setType(new String(chars, i, i1));
                System.out.println(new String(chars, i, i1));
            }
            
             if (selectedBalise.equals("nom")) {
                seclectedMatch.setNom(new String(chars, i, i1));
                System.out.println(new String(chars, i, i1));
            }
              if (selectedBalise.equals("prenom")) {
                seclectedMatch.setPrenom(new String(chars, i, i1));
                System.out.println(new String(chars, i, i1));
              }
                if (selectedBalise.equals("id_tournoi")) {
                seclectedMatch.setId_terrain(Integer.parseInt(new String(chars, i, i1)));
                System.out.println(new String(chars, i, i1));
            }
                
            
             
              
              
              
        }
    }
    
}
