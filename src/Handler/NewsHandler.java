/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Handler;

import Entities.News;
import java.util.Vector;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 *
 * @author Raed
 */
public class NewsHandler extends DefaultHandler {

    private Vector news;

    public NewsHandler() {
        news = new Vector();
    }
    public News[] getNews() {
        News[] newsTab = new News[news.size()];
        news.copyInto(newsTab);
        return newsTab;
    }
    String selectedBalise = "";
    News seclectedNews;

    public void startElement(String string, String string1, String qName, Attributes atrbts) throws SAXException {
        if (qName.equals("news")) {
            seclectedNews = new News();
        } else if (qName.equals("id")) {
            selectedBalise = "id";
        } else if (qName.equals("titre")) {
            selectedBalise = "titre";
        } else if (qName.equals("contenu")) {
            selectedBalise = "contenu";
        } else if (qName.equals("photo")) {
            selectedBalise = "photo";
        } else if (qName.equals("categorie")) {
            selectedBalise = "categorie";
        } else if (qName.equals("mot_cle")) {
            selectedBalise = "mot_cle";
        } else if (qName.equals("date")) {
            selectedBalise = "date";
        }
    }

    public void endElement(String string, String string1, String qName) throws SAXException {
        if (qName.equals("news")) {
            news.addElement(seclectedNews);
            System.out.println(seclectedNews);
            seclectedNews = null;
        } else if (qName.equals("id")) {
            selectedBalise = "";
        } else if (qName.equals("titre")) {
            selectedBalise = "";
        } else if (qName.equals("contenu")) {
            selectedBalise = "";
        } else if (qName.equals("photo")) {
            selectedBalise = "";
        } else if (qName.equals("categorie")) {
            selectedBalise = "";
        } else if (qName.equals("mot_cle")) {
            selectedBalise = "";
        } else if (qName.equals("date")) {
            selectedBalise = "";
        }

    }

    public void characters(char[] chars, int i, int i1) throws SAXException {
        if (seclectedNews != null) {
            if (selectedBalise.equals("id")) {
                seclectedNews.setId(Integer.parseInt(new String(chars, i, i1)));
            }
            if (selectedBalise.equals("titre")) {
                seclectedNews.setTitre(new String(chars, i, i1));
            }
            if (selectedBalise.equals("contenu")) {
                seclectedNews.setContenu(new String(chars, i, i1));
            }
            if (selectedBalise.equals("photo")) {
                seclectedNews.setImage(new String(chars, i, i1));
            }
            if (selectedBalise.equals("categorie")) {
                seclectedNews.setCategorie(new String(chars, i, i1));
            }
            if (selectedBalise.equals("mot_cle")) {
                seclectedNews.setMot_clee(new String(chars, i, i1));
            }
            if (selectedBalise.equals("date")) {
                //seclectedNews.setDate(new String(chars, i, i1));
            }

        }
    }
}
