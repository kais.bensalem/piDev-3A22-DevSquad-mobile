/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Handler;

import Entities.Joueur;
import java.util.Vector;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 *
 * @author ferid
 */
public class JoueurHandler  extends DefaultHandler{
      private Vector joueurVector;
      
      public JoueurHandler () {
          joueurVector =new Vector ();
      }
      
       public Joueur[] getJoueur() {
        Joueur[] personTab = new Joueur[joueurVector.size()];
        joueurVector.copyInto(personTab);
        return personTab;
    }
        String selectedBalise = "";
    Joueur seclectedMatch;
    
        public void startElement(String string, String string1, String qName, Attributes atrbts) throws SAXException {
        if (qName.equals("joueur")) {
            seclectedMatch = new Joueur();
        } else if (qName.equals("id")) {
            selectedBalise = "id";
        } else if (qName.equals("nom")) {
            selectedBalise = "nom";
        } else if (qName.equals("prenom")) {
            selectedBalise = "prenom";
        }  else if (qName.equals("adresse")) {
            selectedBalise = "adresse";
        } else if (qName.equals("statut_joueur")) {
            selectedBalise = "statut_joueur";
        } else if (qName.equals("rang")) {
            selectedBalise = "rang";
        } else if (qName.equals("sexe")) {
            selectedBalise = "sexe";
        }  else if (qName.equals("nationalite")) {
            selectedBalise = "nationalite";
        }   else if (qName.equals("date_naissance")) {
            selectedBalise = "date_naissance";
        }     else if (qName.equals("photo")) {
            selectedBalise = "photo";
        }
        
        
    }
        
            public void endElement(String string, String string1, String qName) throws SAXException {
        if (qName.equals("joueur")) {

            joueurVector.addElement(seclectedMatch);
            seclectedMatch = null;
        } else if (qName.equals("id")) {
            selectedBalise = "id";
        } else if (qName.equals("nom")) {
            selectedBalise = "nom";
        } else if (qName.equals("prenom")) {
            selectedBalise = "prenom";
        }    else if (qName.equals("adresse")) {
            selectedBalise = "adresse";
        }  else if (qName.equals("date_naissance")) {
            selectedBalise = "date_naissance";
        } else if (qName.equals("sexe")) {
            selectedBalise = "sexe";
        }  else if (qName.equals("nationalite")) {
            selectedBalise = "nationalite";
        } else if (qName.equals("rang")) {
            selectedBalise = "rang";
        } else if (qName.equals("statut_joueur")) {
            selectedBalise = "statut_joueur";
        } else if (qName.equals("photo")) {
            selectedBalise = "photo";
        }
        
        
        
    }
                public void characters(char[] chars, int i, int i1) throws SAXException {
        if (seclectedMatch != null) {
            if (selectedBalise.equals("id")) {
                seclectedMatch.setId(Integer.parseInt(new String(chars, i, i1)));
                System.out.println(new String(chars, i, i1));
            }
            if (selectedBalise.equals("nom")) {
                seclectedMatch.setNom(new String(chars, i, i1));
                System.out.println(new String(chars, i, i1));
            }
            if (selectedBalise.equals("prenom")) {
                seclectedMatch.setPrenom(new String(chars, i, i1));
                System.out.println(new String(chars, i, i1));
            }
            
             if (selectedBalise.equals("adresse")) {
                seclectedMatch.setAdresse(new String(chars, i, i1));
                System.out.println(new String(chars, i, i1));
            }
             
              if (selectedBalise.equals("date_naissance")) {
                seclectedMatch.setDate_naissance(new String(chars, i, i1));
                System.out.println(new String(chars, i, i1));
            }
              
                if (selectedBalise.equals("sexe")) {
                seclectedMatch.setSexe(new String(chars, i, i1));
                System.out.println(new String(chars, i, i1));
            }
                  if (selectedBalise.equals("nationalite")) {
                seclectedMatch.setNationalite(new String(chars, i, i1));
                System.out.println(new String(chars, i, i1));
            }
            
                  if (selectedBalise.equals("rang")) {
                seclectedMatch.setRang(Long.parseLong(new String(chars, i, i1)));
                System.out.println(new String(chars, i, i1));
            }
                      if (selectedBalise.equals("statut_joueur")) {
                seclectedMatch.setStatut_joueur(new String(chars, i, i1));
                System.out.println(new String(chars, i, i1));
            }
            
                   if (selectedBalise.equals("photo")) {
                seclectedMatch.setPhoto(new String(chars, i, i1));
                System.out.println(new String(chars, i, i1));
            }
            
             
            
             
              
              
              
        }
    }
    
}
