/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Handler;


import Entities.MiseANiveau;
import java.util.Vector;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 *
 * @author Salamandar
 */
public class MiseANiveauHandler extends DefaultHandler {

    private Vector miseANiveauVector;

    public MiseANiveauHandler() {
        miseANiveauVector = new Vector();
    }

    public MiseANiveau[] getMiseANiveau() {
        MiseANiveau[] miseANiveauTab = new MiseANiveau[miseANiveauVector.size()];
        miseANiveauVector.copyInto(miseANiveauTab);
        return miseANiveauTab;
    }
    String selectedBalise = "";
    MiseANiveau seclectedMiseANiveau;

    public void startElement(String string, String string1, String qName, Attributes atrbts) throws SAXException {
        if (qName.equals("MiseANiveau")) {
            seclectedMiseANiveau = new MiseANiveau();
        } else if (qName.equals("id_event")) {
            selectedBalise = "id_event";
        } else if (qName.equals("nom")) {
            selectedBalise = "nom";
        } else if (qName.equals("date_ouverture")) {
            selectedBalise = "date_ouverture";
        } else if (qName.equals("date_cloture")) {
            selectedBalise = "date_cloture";
        } else if (qName.equals("emplacement")) {
            selectedBalise = "emplacement";
        } else if (qName.equals("nb_participant")) {
            selectedBalise = "nb_participant";
        }

    }

    public void endElement(String string, String string1, String qName) throws SAXException {
        if (qName.equals("MiseANiveau")) {

           miseANiveauVector.addElement(seclectedMiseANiveau);
            seclectedMiseANiveau = null;
        } else if (qName.equals("id_event")) {
            selectedBalise = "id_event";
        } else if (qName.equals("nom")) {
            selectedBalise = "nom";
        } else if (qName.equals("date_ouverture")) {
            selectedBalise = "date_ouverture";
        } else if (qName.equals("date_cloture")) {
            selectedBalise = "date_cloture";
        } else if (qName.equals("emplacement")) {
            selectedBalise = "emplacement";
        } else if (qName.equals("nb_participant")) {
            selectedBalise = "nb_participant";
        }
    }

    public void characters(char[] chars, int i, int i1) throws SAXException {
        if (seclectedMiseANiveau != null) {
            if (selectedBalise.equals("id_event")) {
                seclectedMiseANiveau.setId(Integer.parseInt(new String(chars, i, i1)));
                System.out.println(new String(chars, i, i1));
            }
            if (selectedBalise.equals("nom")) {
                seclectedMiseANiveau.setNom(new String(chars, i, i1));
                System.out.println(new String(chars, i, i1));
            }
            if (selectedBalise.equals("date_ouverture")) {
                seclectedMiseANiveau.setDate_ouverture(new String(chars, i, i1));
                System.out.println(new String(chars, i, i1));
            }

            if (selectedBalise.equals("date_cloture")) {
                seclectedMiseANiveau.setDate_cloture(new String(chars, i, i1));
                System.out.println(new String(chars, i, i1));
            }

            if (selectedBalise.equals("emplacement")) {
                seclectedMiseANiveau.setEmplacement(new String(chars, i, i1));
                System.out.println(new String(chars, i, i1));
            }

            if (selectedBalise.equals("nb_participant")) {
                seclectedMiseANiveau.setNb_participant(Integer.parseInt(new String(chars, i, i1)));
                System.out.println(new String(chars, i, i1));
            }

        }
    }

}
