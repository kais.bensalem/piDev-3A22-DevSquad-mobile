/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Handler;


import Entities.Game;
import java.util.Vector;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 *
 * @author Salamandar
 */
public class PropreMatchHandler extends DefaultHandler {
private Vector matchVector;

    public PropreMatchHandler() {
        matchVector = new Vector();
    }

    public Game[] getMatch() {
        Game[] matchTab = new Game[matchVector.size()];
        matchVector.copyInto(matchTab);
        return matchTab;
    }
    String selectedBalise = "";
    Game seclectedMatch;


    public void startElement(String string, String string1, String qName, Attributes atrbts) throws SAXException {
        if (qName.equals("Match")) {
            seclectedMatch = new Game();
        } else if (qName.equals("id")) {
            selectedBalise = "id";
        } else if (qName.equals("Arbitre")) {
            selectedBalise = "Arbitre";
        } else if (qName.equals("terrain")) {
            selectedBalise = "terrain";
        } else if (qName.equals("date")) {
            selectedBalise = "date";
        } 
    }

    public void endElement(String string, String string1, String qName) throws SAXException {
        if (qName.equals("Match")) {

            matchVector.addElement(seclectedMatch);
            seclectedMatch = null;
        } else if (qName.equals("id")) {
            selectedBalise = "id";
        } else if (qName.equals("Arbitre")) {
            selectedBalise = "Arbitre";
        } else if (qName.equals("terrain")) {
            selectedBalise = "terrain";
        } else if (qName.equals("date")) {
            selectedBalise = "date";
        }
    }

    public void characters(char[] chars, int i, int i1) throws SAXException {
        if (seclectedMatch != null) {
            if (selectedBalise.equals("id")) {
                seclectedMatch.setIdM(Integer.parseInt(new String(chars, i, i1)));
                System.out.println(new String(chars, i, i1));
            }
            if (selectedBalise.equals("Arbitre")) {
                seclectedMatch.setArbitre(new String(chars, i, i1));
                System.out.println(new String(chars, i, i1));
            }
            if (selectedBalise.equals("terrain")) {
                seclectedMatch.setTerain(new String(chars, i, i1));
                System.out.println(new String(chars, i, i1));
            }

            if (selectedBalise.equals("date")) {
                seclectedMatch.setDate(new String(chars, i, i1));
                System.out.println(new String(chars, i, i1));

            }

        }
    
}
}
