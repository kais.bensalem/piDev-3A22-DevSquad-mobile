/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import DAO.NewsDao;
import Entities.News;
import java.io.IOException;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Font;
import javax.microedition.lcdui.Form;
import javax.microedition.lcdui.StringItem;
import javax.microedition.lcdui.Ticker;
import javax.xml.parsers.ParserConfigurationException;
import org.beanizer.j2me.charts.ChartItem;
import org.beanizer.j2me.charts.PieChart;
import org.xml.sax.SAXException;
import pidevmob.Midlet;

/**
 *
 * @author Raed
 */
public class StatistiquesNews extends Form implements CommandListener {

    PieChart mypie = new PieChart("News");
    News[] news;
    int h, s, r, c;
    Command cmd_back = new Command("Retour", Command.SCREEN, 0);

    public StatistiquesNews(String title) {
        super("Statistiques");
        addCommand(cmd_back);
        setCommandListener(this);
        //disp.setCurrent(new Authentification("au"));
        //disp.setCurrent(new ListedesNews());
        try {
            news = new NewsDao().select();
            h = new NewsDao().getByCategorie("Horror").length;
            s = new NewsDao().getByCategorie("Sport").length;
            r = new NewsDao().getByCategorie("Romance").length;
            c = new NewsDao().getByCategorie("Comedy").length;
        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (SAXException ex) {
            ex.printStackTrace();
        } catch (ParserConfigurationException ex) {
            ex.printStackTrace();
        }
        Ticker ticker = new Ticker("Total News : " + news.length + "     Horreur : " + h + "    Sport : " + s + "    Romance : " + r + "    Comedy : " + c);
        initItem(mypie);
        this.setTicker(ticker);
        mypie.setFill(true);
        this.append(mypie);
        StringItem mytext = new StringItem("Note :", "Notre statistiques est baser seulement sur les categories des news");
        this.append(mytext);
    }

    private void initItem(ChartItem item) {
        item.setFont(Font.FACE_PROPORTIONAL, Font.STYLE_PLAIN, Font.SIZE_SMALL);
        item.setDrawAxis(true);
        item.setPreferredSize(this.getWidth(), this.getWidth() - 50);
        item.setMargins(15, 15, 15, 25);

//        try {
//            Image img = Image.createImage("/ressources/airplane.png");
//            ///org/beanizer/j2me/img/image3.png
//            item.setBackgroundImage(img);
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
        item.showShadow(true);
        item.setShadowColor(20, 20, 20);
        item.setColor(40, 40, 200);
        item.resetData();
        item.addElement("Sport", s, 130, 100, 220);
        item.addElement("Horror", h, 220, 20, 20);
        item.addElement("Romance", r, 220, 120, 20);
        item.addElement("Comedy", c, 0, 200, 50);
        //item.setMaxValue(100);

    }

    public void commandAction(Command c, Displayable d) {
        if (c == cmd_back) {
            Midlet.mid.disp.setCurrent(new ListedesNews());
        }
    }

}
