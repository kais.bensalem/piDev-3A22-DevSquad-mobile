/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import DAO.UtilisateurDao;
import Entities.Utilisateur;
import java.io.IOException;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Form;
import javax.microedition.lcdui.TextField;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;
import pidevmob.Midlet;
import pidevmob.Midlet;

/**
 *
 * @author ferid
 */
public class ChercherUtilisateur extends Form implements CommandListener, Runnable {

    TextField tflogin = new TextField("Login", "", 10, TextField.ANY);
    Command cmd_chercher = new Command("Chercher", Command.SCREEN, 0);
//    Command cmdExit = new Command("Exit", Command.EXIT, 0);
    Command cmd_back = new Command("Retour", Command.SCREEN, 0);

    public ChercherUtilisateur(String title) {
        super("Chercher");
        append(tflogin);
        addCommand(cmd_chercher);
//        addCommand(cmdExit);
        addCommand(cmd_back);
        setCommandListener(this);

    }

    public void commandAction(Command c, Displayable d) {
        if (c == cmd_chercher) {
            Thread th = new Thread(this);
            th.start();
        }

        if (c == cmd_back) {
            Midlet.mid.disp.setCurrent(new AdminMenu());

        }
//        if (c == cmdExit) {
//            Midlet.mid.notifyDestroyed();
//        }

    }

    public void run() {
        String strlogin = tflogin.getString();
        Utilisateur[] utilisateurs;
        try {
            utilisateurs = new UtilisateurDao().ChercherUtilisateur(strlogin);
            if (utilisateurs.length == 1) {
                System.out.println(utilisateurs[0]);
                Midlet.mid.disp.setCurrent(new Supprimer_bannirUtilisateur(strlogin, utilisateurs[0]));

            }
        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (SAXException ex) {
            ex.printStackTrace();
        } catch (ParserConfigurationException ex) {
            ex.printStackTrace();
        }

    }

}
