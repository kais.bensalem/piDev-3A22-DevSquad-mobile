/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import DAO.JoueurDao;
import Entities.Joueur;
import java.io.IOException;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Image;
import javax.microedition.lcdui.ImageItem;
import javax.microedition.lcdui.List;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;
import pidevmob.Midlet;

/**
 *
 * @author ferid
 */
public class ListedesJoueurs extends List implements CommandListener,Runnable{
    int id_j;
     // Command cmdExit = new Command("Exit", Command.EXIT, 0);
      Command cmd_back = new Command("Retour", Command.SCREEN, 0);
    public ListedesJoueurs() {
        
         super("Liste des joueurs", List.IMPLICIT);
       // addCommand(cmdExit);
        addCommand(cmd_back);
        setCommandListener(this);
        
        Thread th = new Thread(this);
        th.start();
           
                       
                      
    }
    public ListedesJoueurs(int id) {
        
         super("Liste des joueurs", List.IMPLICIT);
         this.id_j=id;
        //addCommand(cmdExit);
        addCommand(cmd_back);
        setCommandListener(this);
        
        Thread th = new Thread(this);
        th.start();
           
                       
                      
    }

    public void commandAction(Command c, Displayable d) {
//    if (c == cmdExit) {
//            Midlet.mid.notifyDestroyed();
//        } 
      if (c==cmd_back) {
     if(id_j==1)
     {
           Midlet.mid.disp.setCurrent(new AdminMenu());
     }
     else  if(id_j==2)
     {
           Midlet.mid.disp.setCurrent(new JoueurMenu());
     }
     else  if(id_j==3)
     {
           Midlet.mid.disp.setCurrent(new UtilisateurMenu());
     }
     }
    
    }

    public void run() {
          try {
              Joueur[] joueurs = new JoueurDao().select();
             
              if (joueurs.length > 0) {
                  for (int i = 0; i < joueurs.length; i++) {
                       System.out.println(joueurs[i]+"lllllll"+joueurs[i].getStatut_joueur());
                      append("\n***********************************",null);
                      append("Le numero "+joueurs[i].getRang()+" du classement", null);
                      append("***********************************",null);
                      System.out.println(joueurs[i]);
                    
                      
                       
                       append("Nom : "+joueurs[i].getNom(),null);
                        append("Prenom : "+joueurs[i].getPrenom(),null);
                         append("D.Naissance : " +joueurs[i].getDate_naissance(),null);
                          append("Sexe : "+joueurs[i].getSexe(),null);
                           append("Pays : "+joueurs[i].getNationalite(),null);
                            append("Adresse : "+joueurs[i].getAdresse(),null);
                           // if (joueurs[i].getStatut_joueur()==null) {joueurs[i].setStatut_joueur("Non mentionne");}
                             append("Statut : "+joueurs[i].getStatut_joueur(),null);
                     
                  }
              } } catch (IOException ex) {
              ex.printStackTrace();
          } catch (SAXException ex) {
              ex.printStackTrace();
          } catch (ParserConfigurationException ex) {
              ex.printStackTrace();
          }
    }
    
}
