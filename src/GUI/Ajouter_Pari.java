/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import DAO.PariDao;
import DAO.ReservationDao;
import Entities.Match;
import Entities.Pari;
import Entities.Reservation;
import Entities.Utilisateur;
import java.io.IOException;
import javax.microedition.lcdui.Alert;
import javax.microedition.lcdui.AlertType;
import javax.microedition.lcdui.ChoiceGroup;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Form;
import javax.microedition.lcdui.StringItem;
import javax.microedition.lcdui.TextField;
import pidevmob.Midlet;

/**
 *
 * @author Raed
 */
public class Ajouter_Pari extends Form implements CommandListener {
    String T[] = new String[2];
    Match m;
    StringItem s1 = new StringItem("Match : ", null);
    TextField montant = new TextField("Montant", null, 20, TextField.NUMERIC);
    ChoiceGroup cg ;
    Command ok = new Command("OK", Command.SCREEN, 0);
    Command back = new Command("Back", Command.SCREEN, 0);
    Alert a = new Alert("Succee", "Pari avec Succee", null, AlertType.INFO);

    public Ajouter_Pari(String title, Match m, Match m2) {
        super(title);
        a.setTimeout(5000);
        T[0] = m.getNom();
        T[1] = m2.getNom();
        cg = new ChoiceGroup("Votre Choix", ChoiceGroup.POPUP,T , null);
        s1.setText("Match : " + m.getNom() + " VS " + m2.getNom());
        this.m = m;
        this.append(s1);
        this.append(cg);
        this.append(montant);
        addCommand(ok);
        addCommand(back);
        setCommandListener(this);

    }

    public void commandAction(Command c, Displayable d) {

        if (c == ok) {
            PariDao pdao = new PariDao();
            Pari p = new Pari(1, new Utilisateur(8, null, null, null, null), Integer.parseInt(montant.getString()), 1 , 0);
          try {
                pdao.ajouter(p);
                Midlet.mid.disp.setCurrent(a, new ListedesMatchs(1));
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        if (c == back) {
            Midlet.mid.disp.setCurrent(new ListedesMatchs(1));
        }
    }

}
