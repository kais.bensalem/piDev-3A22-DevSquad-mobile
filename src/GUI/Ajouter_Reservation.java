/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import DAO.ReservationDao;
import Entities.Match;
import Entities.Reservation;
import Entities.Utilisateur;
import java.io.IOException;
import javax.microedition.lcdui.Alert;
import javax.microedition.lcdui.AlertType;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Form;
import javax.microedition.lcdui.StringItem;
import javax.microedition.lcdui.TextField;
import pidevmob.Midlet;

/**
 *
 * @author Raed
 */
public class Ajouter_Reservation extends Form implements CommandListener {

    Match m;
    StringItem s1 = new StringItem("Match : ", null);
    TextField cat1 = new TextField("Categorie 1", null, 20, TextField.NUMERIC);
    TextField cat2 = new TextField("Categorie 2", null, 20, TextField.NUMERIC);
    TextField cat3 = new TextField("Categorie 3", null, 20, TextField.NUMERIC);
    TextField cat4 = new TextField("Categorie 4", null, 20, TextField.NUMERIC);
    Command ok = new Command("OK", Command.SCREEN, 0);
    Command back = new Command("Back", Command.SCREEN, 0);
    Alert a = new Alert("Succee", "Reservation avec Succee", null, AlertType.INFO);

    public Ajouter_Reservation(String title, Match m, Match m2) {
        super(title);
        a.setTimeout(5000);
        s1.setText("Match : " + m.getNom() + " VS " + m2.getNom());
        this.m = m;
        this.append(s1);
        this.append(cat1);
        this.append(cat2);
        this.append(cat3);
        this.append(cat4);
        addCommand(ok);
        addCommand(back);
        setCommandListener(this);

    }

    public void commandAction(Command c, Displayable d) {

        if (c == ok) {
            ReservationDao rdao = new ReservationDao();
            Reservation r = new Reservation(1, new Utilisateur(8, null, null, null, null), m, true, Integer.parseInt(cat1.getString()), Integer.parseInt(cat2.getString()), Integer.parseInt(cat3.getString()), Integer.parseInt(cat4.getString()), 500);
            try {
                rdao.ajouter(r);
                Midlet.mid.disp.setCurrent(a, new ListedesMatchs(1));
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        if (c == back) {
            Midlet.mid.disp.setCurrent(new ListedesMatchs(1));
        }
    }

}
