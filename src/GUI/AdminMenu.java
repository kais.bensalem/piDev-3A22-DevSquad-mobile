/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import java.io.IOException;
import javax.microedition.lcdui.Canvas;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.Image;
import pidevmob.Midlet;

/**
 *
 * @author ferid
 */
public class AdminMenu extends Canvas implements CommandListener {

    int position = 1;
    Image img;

    Command cmd_deco = new Command("Deconnexion", Command.SCREEN, 0);
    protected void paint(Graphics g) {
        int w = getWidth();
        int h = getHeight();
        g.fillRect(0, 0, w, h);
        try {
            img = Image.createImage("/Image/menuadmin.png");
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        addCommand(cmd_deco);
        setCommandListener(this);
        g.drawImage(img, 0, 0, 0);
        g.setColor(255, 0, 0);
        g.drawString("Arbitre", 20, 55, 0);
        g.drawString("Clubs", 100, 55, 0);
        g.drawString("Events", 175, 55, 0);
        g.drawString("Joueurs", 20, 125, 0);
        g.drawString("Utilisateurs", 85, 125, 0);
        g.drawString("Paris", 175, 125, 0);
        g.drawString("Reservation", 5, 200, 0);
        g.drawString("News", 100, 200, 0);
        g.drawString("Medecins", 170, 200, 0);
        g.drawString("Nv.Inscris", 17, 275, 0);
        g.drawString("Terrains", 95, 275, 0);
        g.drawString("Matches", 170, 275, 0);
        switch (position) {

            case 1:
                g.drawRect(25, 15, 40, 40);

                break;
            case 2:
                g.drawRect(100, 15, 40, 40);
                break;
            case 3:
                g.drawRect(175, 15, 40, 40);

                break;
            case 4:
                g.drawRect(25, 85, 40, 40);

                break;
            case 5:
                g.drawRect(100, 85, 40, 40);

                break;
            case 6:
                g.drawRect(175, 85, 40, 40);

                break;
            case 7:
                g.drawRect(25, 160, 40, 40);

                break;
            case 8:
                g.drawRect(100, 160, 40, 40);

                break;
            case 9:
                g.drawRect(175, 160, 40, 40);

                break;
            case 10:
                g.drawRect(25, 235, 40, 40);

                break;
            case 11:
                g.drawRect(100, 235, 40, 40);

                break;
            case 12:
                g.drawRect(175, 235, 40, 40);

                break;

        }
    }

    protected void keyPressed(int keyCode) {

        int gameAction = getGameAction(keyCode);

        if (gameAction == LEFT && position > 1) {
            position--;
        } else if (gameAction == RIGHT && position < 12) {
            position++;
        } else if (gameAction == UP && position > 3) {
            position -= 3;
        } else if (gameAction == DOWN && position < 10) {
            position += 3;
        }
        if (gameAction == FIRE) {
            switch (position) {
                case 1:
                    Midlet.mid.disp.setCurrent(new AdminMenu());
                    break;
                case 2:
                    Midlet.mid.disp.setCurrent(new AdminMenu());
                    break;
                case 3:
                    Midlet.mid.disp.setCurrent(new ListedesTournoi());
                    break;
                case 4:
                    Midlet.mid.disp.setCurrent(new ListedesJoueurs(1));
                    break;
                case 5:
                    Midlet.mid.disp.setCurrent(new ChercherUtilisateur(""));
                    break;
                case 6:
                    //     Midlet.mid.disp.setCurrent(new ListedesParis());
                    break;
                case 7:
                    Midlet.mid.disp.setCurrent(new ListedesReservations());
                    break;
                case 8:
                    Midlet.mid.disp.setCurrent(new ListedesNews(1));
                    break;
                case 9:
                    Midlet.mid.disp.setCurrent(new ListedesMedecins());
                    break;
                case 10:
                    Midlet.mid.disp.setCurrent(new ListedesNouveauxInscri());
                    break;
                case 11:
                    //    Midlet.mid.disp.setCurrent(new ListedesTerrain());
                    break;
                case 12:
                    Midlet.mid.disp.setCurrent(new ListedesMatchs(1));
                    break;

            }

        }
        repaint();
    }

    public void commandAction(Command c, Displayable d) {
        if (c==cmd_deco){
            Midlet.mid.disp.setCurrent(new Authentification(""));
        }
    }

}
