/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import DAO.NewsDao;
import DAO.ReservationDao;
import Entities.News;
import Entities.Reservation;
import java.io.IOException;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Image;
import javax.microedition.lcdui.List;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;
import pidevmob.Midlet;

/**
 *
 * @author Raed
 */
public class ListedesReservations extends List implements CommandListener, Runnable {

    Command cmd_back = new Command("Retour", Command.SCREEN, 0);

    Reservation[] reservations;
//    Image img;
    public ListedesReservations() {
        super("Liste des Reservations", List.IMPLICIT);
//        try {
//            img = Image.createImage("/ressources/airplane.png");
//        } catch (IOException ex) {
//            ex.getMessage();
//        }
        addCommand(cmd_back);

        setCommandListener(this);
        Thread th = new Thread(this);
        th.start();
    }

    public void commandAction(Command c, Displayable d) {

        if (c == cmd_back) {
            Midlet.mid.disp.setCurrent(new AdminMenu());
        }
        
    }

    public void run() {
        try {
            reservations = new ReservationDao().select();
            if (reservations.length > 0) {
                setTitle("Liste des Reservations [" + reservations.length + "]");
                for (int i = 0; i < reservations.length; i++) {
                    append(reservations[i].getU().getId() + "\n" + reservations[i].getM().getId_match()+"\n" + reservations[i].getPrix() , null);
                    append("\n***********************************", null);
                }
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (SAXException ex) {
            ex.printStackTrace();
        } catch (ParserConfigurationException ex) {
            ex.printStackTrace();
        }
    }
}
