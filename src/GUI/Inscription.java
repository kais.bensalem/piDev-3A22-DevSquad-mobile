/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import DAO.UtilisateurDao;
import Entities.Utilisateur;
import java.io.DataInputStream;
import java.io.IOException;
import javax.microedition.io.Connector;
import javax.microedition.io.HttpConnection;
import javax.microedition.lcdui.Alert;
import javax.microedition.lcdui.AlertType;
import javax.microedition.lcdui.ChoiceGroup;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.DateField;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Form;
import javax.microedition.lcdui.TextField;
import pidevmob.Midlet;

/**
 *
 * @author ferid
 */
public class Inscription  extends Form  implements CommandListener,Runnable{

    TextField tflogin=new TextField("Login\n", "", 10, TextField.ANY);
    TextField tfpassword=new TextField("Password\n", "", 10, TextField.PASSWORD);
    TextField tfcin=new TextField("CIN\n", "", 10, TextField.ANY);
    TextField tfnom=new TextField("Nom\n", "", 10, TextField.ANY);
    TextField tfprenom=new TextField("Prenom\n", "", 10, TextField.ANY);
    TextField tfemail=new TextField("Email\n", "", 20, TextField.ANY);
    TextField tftel=new TextField("Telephone\n", "", 10, TextField.NUMERIC);
    TextField tfadresse=new TextField("Adresse\n", "", 10, TextField.ANY);
    
    /*
    TextField tfnation=new TextField("Login\n", "", 10, TextField.ANY);
    TextField tfdate=new TextField("Login\n", "", 10, TextField.ANY);
    TextField tfsexe=new TextField("Login\n", "", 10, TextField.ANY);*/
    DateField dn =new DateField("date", DateField.DATE);
    String [] S ={"M","F"};
          ChoiceGroup sexe =new ChoiceGroup("Sexe", ChoiceGroup.POPUP, S, null);
    String [] N ={"AF","FR","IT","TN"};
          ChoiceGroup nationalite =new ChoiceGroup("Pays", ChoiceGroup.POPUP, N, null);
    
         Command cmdExit = new Command("Exit", Command.EXIT, 0);   
     Command cmd_inscri = new Command("S'inscrire", Command.SCREEN, 0);
      Command cmd_back = new Command("Retour", Command.SCREEN, 0);
      
      String mail="" ;
    public Inscription(String title) {
        super("Inscription");
        append(tflogin);
        append(tfpassword);
        tfcin.setMaxSize(8);
        append(tfcin);
        append(tfnom);
        append(tfprenom);
        append(tfemail);
        append(dn);/***/
        append(sexe);/***/
        append(nationalite);/****/
        append(tftel);
        append(tfadresse);
        addCommand(cmdExit);
         addCommand(cmd_inscri);
         addCommand(cmd_back);
        setCommandListener(this);
    }

    public void commandAction(Command c, Displayable d) {
    if (c==cmd_inscri) {
    Thread th = new Thread(this);
            th.start();
       
    
    } 
     if (c == cmdExit) {
            Midlet.mid.notifyDestroyed();
        }
     if (c==cmd_back) {
       Midlet.mid.disp.setCurrent(new Authentification(""));
     }
    
    }

    public void run() {
     String login=tflogin.getString();
       String password= tfpassword.getString();
        String cin=tfcin.getString();
        String nom =tfnom.getString();
        String prenom=tfprenom.getString();
       String email=tfemail.getString();
//        append(dn);/***/
        
       
        long tel =Long.parseLong(tftel.getString());
        String adresse=tfadresse.getString();
         String nat="";
        for (int i=0;i<nationalite.size();i++) {
         if (nationalite.isSelected(i)) {
         nat+=nationalite.getString(i);
         }
         }
        System.out.println(nat);
      String s="";
        for (int i=0;i<sexe.size();i++) {
         if (sexe.isSelected(i)) {
         s+=sexe.getString(i);
         }
         }
        System.out.println(s+"##############################");
        Utilisateur u =   new Utilisateur( nom,  prenom, login,  password, cin,adresse, email, "1993-12-04", s, tel,  nat);
        System.out.println(u);
        boolean result = new UtilisateurDao().insert(u );
       mail=email;
     System.out.println("apres**********************");
     Alert alert = new Alert("Résultat");
        if (result) {
          
             try {
             sendMail(mail);
         } catch (IOException ex) {
             ex.printStackTrace();
         }
            alert.setType(AlertType.CONFIRMATION);
            alert.setString("Inscription avec succes");
           Midlet.mid.disp.setCurrent(new Authentification(""));
            Midlet.mid.disp.setCurrent(alert);
          
            
        } else {
            alert.setType(AlertType.ERROR);
            alert.setString("Erreur d'inscriprion");
            Midlet.mid.disp.setCurrent(alert);
        }
    }
        private String sendMail(String mail) throws IOException {
        String resa = "";

//        String urlX = "http://localhost/CMDmobile/sendMail.php?to=" +  tfto.getString() + "&sub=" + tfsub.getString() + "&message=" + tfmes.getString().replace(' ', ' ')+ "&from=" + tffrom.getString() + "&mot=" + tfpwd.getString() ;
         String urlX = "http://localhost/MobilePidev/CMDmobile/sendMail.php?to="+mail;
        HttpConnection hm = (HttpConnection) Connector.open(urlX);
        DataInputStream dis = new DataInputStream(hm.openDataInputStream());

        return resa;

    }
}
