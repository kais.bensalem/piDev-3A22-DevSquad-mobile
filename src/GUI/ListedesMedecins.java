/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import DAO.MedecinDao;
import Entities.Medecin;
import java.io.IOException;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Image;
import javax.microedition.lcdui.ImageItem;
import javax.microedition.lcdui.List;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;
import pidevmob.Midlet;

/**
 *
 * @author ferid
 */
public class ListedesMedecins extends List implements CommandListener, Runnable {

    //Command cmdExit = new Command("Exit", Command.EXIT, 0);
    Command cmd_back = new Command("Retour", Command.SCREEN, 0);

    public ListedesMedecins() {

        super("Liste des medecins", List.IMPLICIT);
        //addCommand(cmdExit);
        addCommand(cmd_back);
        setCommandListener(this);

        Thread th = new Thread(this);
        th.start();

    }

    public void commandAction(Command c, Displayable d) {
//    if (c == cmdExit) {
//            Midlet.mid.notifyDestroyed();
//        } 
        if (c == cmd_back) {
            Midlet.mid.disp.setCurrent(new AdminMenu());
        }

    }

    public void run() {
        try {
            Medecin[] medecins = new MedecinDao().select();

            if (medecins.length > 0) {
                for (int i = 0; i < medecins.length; i++) {

                    append("\n***********************************", null);
                    append("Medecin numero:" + (i + 1) + "", null);
                    append("***********************************", null);
                    System.out.println(medecins[i]);

                    append("Nom : " + medecins[i].getNom(), null);
                    append("Prenom : " + medecins[i].getPrenom(), null);
                    append("D.Naissance : " + medecins[i].getDate_naissance(), null);
                    append("Sexe : " + medecins[i].getSexe(), null);
                    append("Pays : " + medecins[i].getNationalite(), null);
                    append("Adresse : " + medecins[i].getAdresse(), null);
                    // if (medecins[i].getStatut_medecin()==null) {medecins[i].setStatut_medecin("Non mentionne");}
                    append("Email : " + medecins[i].getEmail(), null);

                }
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (SAXException ex) {
            ex.printStackTrace();
        } catch (ParserConfigurationException ex) {
            ex.printStackTrace();
        }
    }

}
