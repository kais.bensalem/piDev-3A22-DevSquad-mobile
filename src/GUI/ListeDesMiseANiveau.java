/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import DAO.FormationDao;
import DAO.MiseANiveauDao;
import Entities.Formation;
import Entities.MiseANiveau;
import java.io.IOException;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.List;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;
import pidevmob.Midlet;

/**
 *
 * @author Salamandar
 */
public class ListeDesMiseANiveau extends List implements CommandListener, Runnable {

    Command cmdExit = new Command("Exit", Command.EXIT, 0);
    Command cmd_back = new Command("Retour", Command.SCREEN, 0);

    public ListeDesMiseANiveau() {

        super("liste mise a niveau", List.IMPLICIT);
        addCommand(cmdExit);
        addCommand(cmd_back);
        setCommandListener(this);
        Thread th = new Thread(this);
        th.start();
    }

    public void commandAction(Command c, Displayable d) {
        if (c == cmdExit) {
            Midlet.mid.notifyDestroyed();
        }
        if (c == cmd_back) {
         Midlet.mid.disp.setCurrent(new ArbitreMenu());
        }
    }

    public void run() {
        try {
            MiseANiveau[] miseANiveaus = new MiseANiveauDao().select();

            if (miseANiveaus.length > 0) {
                for (int i = 0; i < miseANiveaus.length; i++) {
                    append("\n***********************************",null);
                    append("Nom : " + miseANiveaus[i].getNom(), null);
                    append("Date ouverture : " + miseANiveaus[i].getDate_ouverture(), null);
                    append("Date cloture : " + miseANiveaus[i].getDate_cloture(), null);
                    append("Emplacement : " + miseANiveaus[i].getEmplacement(), null);
                    append("Nombre participant : " + miseANiveaus[i].getNb_participant(), null);
                    append("\n***********************************",null);
                }
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (SAXException ex) {
            ex.printStackTrace();
        } catch (ParserConfigurationException ex) {
            ex.printStackTrace();
        }
    }

}
