/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import DAO.UtilisateurDao;
import Entities.Utilisateur;
import javax.microedition.lcdui.Alert;
import javax.microedition.lcdui.AlertType;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Form;
import javax.microedition.lcdui.StringItem;
import pidevmob.Midlet;

/**
 *
 * @author ferid
 */
public class Supprimer_bannirUtilisateur extends Form implements CommandListener, Runnable {

    Command cmd_supprimer = new Command("Supprimer", Command.SCREEN, 0);
    Command cmd_bannir = new Command("Bannir", Command.SCREEN, 0);
//    Command cmdExit = new Command("Exit", Command.SCREEN, 0);
    String login;
    int choix = 1;
    Command cmd_back = new Command("Retour", Command.SCREEN, 0);

    public Supprimer_bannirUtilisateur(String title, Utilisateur ut) {
        super("Supprimer Utilisateur");
        this.login = ut.getLogin();
        StringItem st_nom = new StringItem("nom: ", ut.getNom());
        StringItem st_prenom = new StringItem("prenom ", ut.getPrenom());
        StringItem st_login = new StringItem("Login", ut.getLogin());
        StringItem st_type = new StringItem("Type", ut.getType());
        append(st_nom);
        append(st_prenom);
        append(st_login);
        append(st_type);
        addCommand(cmd_supprimer);
        addCommand(cmd_bannir);
//        addCommand(cmdExit);
        addCommand(cmd_back);
        setCommandListener(this);
    }

    public void commandAction(Command c, Displayable d) {
        if (c == cmd_supprimer) {
            choix = 1;
            Thread th = new Thread(this);
            th.start();

        }
        if (c == cmd_bannir) {
            choix = 2;
            Thread th = new Thread(this);
            th.start();

        }
//        if (c == cmdExit) {
//            Midlet.mid.notifyDestroyed();
//        }
        if (c == cmd_back) {
            Midlet.mid.disp.setCurrent(new ChercherUtilisateur(""));
        }
    }

    public void run() {

        UtilisateurDao d = new UtilisateurDao();
        if (choix == 1) {
            Alert alert = new Alert("Résultat");
            if (d.supprimer(login)) {
                alert.setType(AlertType.ERROR);
                alert.setString("suppression avec succes");
                alert.setTimeout(2000);
//            Midlet.mid.disp.setCurrent(alert);
                Midlet.mid.disp.setCurrent(new ChercherUtilisateur(login));
                Midlet.mid.disp.setCurrent(alert);
            }
        }

        if (choix == 2) {
            Alert alert = new Alert("Résultat");
            if (d.bannir(login)) {
                alert.setType(AlertType.ERROR);
                alert.setString("Bann  avec succes");
                alert.setTimeout(2000);
//            Midlet.mid.disp.setCurrent(alert);
                Midlet.mid.disp.setCurrent(new ChercherUtilisateur(login));
                Midlet.mid.disp.setCurrent(alert);
            }
        }

    }

}
