/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import DAO.NewsDao;
import Entities.News;
import java.io.IOException;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Image;
import javax.microedition.lcdui.List;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;
import pidevmob.Midlet;

/**
 *
 * @author Raed
 */
public class ListedesNews extends List implements CommandListener, Runnable {

    Command cmdStatisitiques = new Command("Statisitiques", Command.SCREEN, 0);
    Command cmd_back = new Command("Retour", Command.SCREEN, 0);
    Command cmd_read = new Command("Lire", Command.SCREEN, 0);
    int id_j;
    News[] news;
//    Image img;

    public ListedesNews() {
        super("Liste des News", List.IMPLICIT);
//        try {
//            img = Image.createImage("/ressources/airplane.png");
//        } catch (IOException ex) {
//            ex.getMessage();
//        }
        addCommand(cmd_back);
        addCommand(cmd_read);
        addCommand(cmdStatisitiques);

        setCommandListener(this);
        Thread th = new Thread(this);
        th.start();
    }

    public ListedesNews(int id) {
        super("Liste des News", List.IMPLICIT);
//        try {
//            img = Image.createImage("/ressources/airplane.png");
//        } catch (IOException ex) {
//            ex.getMessage();
//        }
        this.id_j = id;
        addCommand(cmd_back);
        addCommand(cmd_read);
        addCommand(cmdStatisitiques);

        setCommandListener(this);
        Thread th = new Thread(this);
        th.start();
    }

    public void commandAction(Command c, Displayable d) {

        if (c == cmd_back) {
            if (id_j == 1) {
                Midlet.mid.disp.setCurrent(new AdminMenu());
            } else if (id_j == 2) {
                Midlet.mid.disp.setCurrent(new ArbitreMenu());
            } else if (id_j == 3) {
                Midlet.mid.disp.setCurrent(new JoueurMenu());
            }else if (id_j == 4) {
                Midlet.mid.disp.setCurrent(new UtilisateurMenu());
            }

        }
        if (c == cmdStatisitiques) {
            Midlet.mid.disp.setCurrent(new StatistiquesNews(null));
        }
        if (c == cmd_read) {
            try {
                Midlet.mid.disp.setCurrent(new LireNews(news[this.getSelectedIndex() / 2]));
            } catch (Exception e) {
            }
        }
    }

    public void run() {
        try {
            news = new NewsDao().select();
            if (news.length > 0) {
                setTitle("Liste des News [" + news.length + "]");
                for (int i = 0; i < news.length; i++) {
                    append("Titre : " + news[i].getTitre() + "\nContenu : " + news[i].getContenu_min() + "\nCategorie : " + news[i].getCategorie() + "\nDate : " + news[i].getDate(), null);
                    append("\n***********************************", null);
                }
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (SAXException ex) {
            ex.printStackTrace();
        } catch (ParserConfigurationException ex) {
            ex.printStackTrace();
        }
    }
}
