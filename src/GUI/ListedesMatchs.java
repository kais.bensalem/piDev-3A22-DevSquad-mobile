/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import DAO.MatchDao;
import Entities.Match;
import java.io.IOException;

import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Image;
import javax.microedition.lcdui.ImageItem;
import javax.microedition.lcdui.Item;
import javax.microedition.lcdui.List;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;
import pidevmob.Midlet;

/**
 *
 * @author ferid
 */
public class ListedesMatchs extends List implements CommandListener, Runnable {

    int id_m;
    //Command cmdExit = new Command("Exit", Command.EXIT, 0);
    Command cmd_back = new Command("Retour", Command.SCREEN, 0);
    Command cmd_reservation = new Command("Reservation", Command.SCREEN, 0);
    Command cmd_pari = new Command("Pari", Command.SCREEN, 0);

    Match[] matchs;
    public ListedesMatchs() {

        super("Liste des Matches", List.IMPLICIT);

        //addCommand(cmdExit);
        addCommand(cmd_back);
        addCommand(cmd_reservation);
        addCommand(cmd_pari);
        setCommandListener(this);

        Thread th = new Thread(this);
        th.start();

    }

    public ListedesMatchs(int id) {

        super("Liste des Matches", List.IMPLICIT);
        this.id_m = id;
        //addCommand(cmdExit);
        addCommand(cmd_back);
        addCommand(cmd_reservation);
        addCommand(cmd_pari);
        setCommandListener(this);

        Thread th = new Thread(this);
        th.start();

    }

    public void commandAction(Command c, Displayable d) {
//        if (c == cmdExit) {
//            Midlet.mid.notifyDestroyed();
//        }
        if (c == cmd_back) {
            if (id_m == 1) {
                Midlet.mid.disp.setCurrent(new AdminMenu());

            } else if (id_m == 2) {
                Midlet.mid.disp.setCurrent(new ArbitreMenu());

            } else if (id_m == 3) {
                Midlet.mid.disp.setCurrent(new UtilisateurMenu());

            }

        }
        if (c == cmd_reservation) {
            Midlet.mid.disp.setCurrent(new Ajouter_Reservation("",matchs[this.getSelectedIndex()],matchs[this.getSelectedIndex() + 1]));
        }
        if (c == cmd_pari) {
            Midlet.mid.disp.setCurrent(new Ajouter_Pari("",matchs[this.getSelectedIndex()],matchs[this.getSelectedIndex() + 1]));
        }

    }

    public void run() {
        try {
            matchs = new MatchDao().select();

            if (matchs.length > 0) {
                for (int i = 0; i < matchs.length; i += 2) {
                    append(matchs[i].getPrenom() + " " + matchs[i].getNom() + " VS " + matchs[i + 1].getPrenom() + " " + matchs[i + 1].getNom() + "\nMatche " + matchs[i].getType() + "\n Date : " + matchs[i].getDate() + "\n***********************************", null);

                }
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (SAXException ex) {
            ex.printStackTrace();
        } catch (ParserConfigurationException ex) {
            ex.printStackTrace();
        }
    }

}
