/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import Entities.News;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.List;
import pidevmob.Midlet;

/**
 *
 * @author Raed
 */
class LireNews extends List implements CommandListener{

    Command cmd_back = new Command("Retour", Command.SCREEN, 0);
    public LireNews(News n) {
        super("Liste des News", List.IMPLICIT);
        addCommand(cmd_back);
        setCommandListener(this);
        this.append(n.getTitre(), null);
        this.append(n.getContenu(), null);
    }

    public void commandAction(Command c, Displayable d) {
        if (c == cmd_back) {
            Midlet.mid.disp.setCurrent(new ListedesNews());
        }
    }
    
    
}
