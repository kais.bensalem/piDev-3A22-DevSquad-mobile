/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import DAO.MatchDao;
import Entities.Joueur;
import Entities.Match;
import java.io.IOException;

import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Image;
import javax.microedition.lcdui.ImageItem;
import javax.microedition.lcdui.Item;
import javax.microedition.lcdui.List;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;
import pidevmob.Midlet;

/**
 *
 * @author ferid
 */
public class ListedesMatchsJoueur extends List implements CommandListener,Runnable{
        Joueur J =new Joueur(2, null, null, null, null, null, null, POPUP, null);
      Command cmdExit = new Command("Exit", Command.EXIT, 0);
      Command cmd_back = new Command("Retour", Command.SCREEN, 0);
    public ListedesMatchsJoueur() {
        
         super("Vos Prochain match", List.IMPLICIT);
        addCommand(cmdExit);
        addCommand(cmd_back);
        setCommandListener(this);
        
        Thread th = new Thread(this);
        th.start();
           
                       
                      
    }

    public void commandAction(Command c, Displayable d) {
    if (c == cmdExit) {
            Midlet.mid.notifyDestroyed();
        } 
      if (c==cmd_back) {
        Midlet.mid.disp.setCurrent(new JoueurMenu());
     }
    
    }

    public void run() {
          try {
              
              Match[] matchs = new MatchDao().selectparjoueur(J);
             
              if (matchs.length > 0) {
                  for (int i = 0; i < matchs.length; i++) {
                      append("vous affrontez "+matchs[i].getPrenom()+" "+matchs[i].getNom()+"\nMatche "+matchs[i].getType()+"\nDate : "+matchs[i].getDate()+"\n***********************************", null);
                     
                  }
              } } catch (IOException ex) {
              ex.printStackTrace();
          } catch (SAXException ex) {
              ex.printStackTrace();
          } catch (ParserConfigurationException ex) {
              ex.printStackTrace();
          }
    }
   
}
