/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import DAO.MatchDao;
import Entities.Joueur;
import Entities.Match;
import java.io.IOException;

import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Image;
import javax.microedition.lcdui.ImageItem;
import javax.microedition.lcdui.Item;
import javax.microedition.lcdui.List;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;
import pidevmob.Midlet;

/**
 *
 * @author ferid
 */
public class ListedesMatchstournoi extends List implements CommandListener, Runnable {

    int id_tournoi;
    //Command cmdExit = new Command("Exit", Command.EXIT, 0);
    Command cmd_back = new Command("Retour", Command.SCREEN, 0);

    public ListedesMatchstournoi(int id) {

        super("Matchs du Tournoi", List.IMPLICIT);
//        addCommand(cmdExit);
        addCommand(cmd_back);
        setCommandListener(this);
        this.id_tournoi = id;
        Thread th = new Thread(this);
        th.start();

    }

    public void commandAction(Command c, Displayable d) {
//    if (c == cmdExit) {
//            Midlet.mid.notifyDestroyed();
//        } 
        if (c == cmd_back) {
            Midlet.mid.disp.setCurrent(new ListedesTournoi());
        }

    }

    public void run() {
        try {

            Match[] matchs = new MatchDao().selectpartournoi(id_tournoi);

            if (matchs.length > 0) {
                for (int i = 0; i < matchs.length; i += 2) {
                    append(matchs[i].getPrenom() + " " + matchs[i].getNom() + " VS " + matchs[i + 1].getPrenom() + " " + matchs[i + 1].getNom() + "\nMatche " + matchs[i].getType() + "\n Date : " + matchs[i].getDate() + "\n***********************************", null);

                }
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (SAXException ex) {
            ex.printStackTrace();
        } catch (ParserConfigurationException ex) {
            ex.printStackTrace();
        }
    }

}
