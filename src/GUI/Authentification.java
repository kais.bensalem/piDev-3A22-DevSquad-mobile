/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import DAO.UtilisateurDao;
import Entities.Utilisateur;
import java.io.IOException;
import javax.microedition.lcdui.Alert;
import javax.microedition.lcdui.AlertType;
import javax.microedition.lcdui.Canvas;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Form;
import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.Image;
import javax.microedition.lcdui.Item;
import javax.microedition.lcdui.TextField;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;
import pidevmob.Midlet;

/**
 *
 * @author ferid
 */
public class Authentification extends Form implements CommandListener, Runnable {

    Image img;
    TextField tflogin = new TextField("Login\n", "", 10, TextField.ANY);
    TextField tfpassword = new TextField("Password\n", "", 10, TextField.PASSWORD);
    Command cmd_cnx = new Command("Connexion", Command.SCREEN, 0);
    Command cmd_inscri = new Command("S'inscrire", Command.SCREEN, 0);
    Command cmdExit = new Command("Exit", Command.EXIT, 0);

    Alert a;

    public Authentification(String title) {
        super("Authentification");
        tflogin.setLayout(Item.LAYOUT_CENTER);
        tfpassword.setLayout(Item.LAYOUT_CENTER);
        tflogin.setMaxSize(10);
        tfpassword.setMaxSize(10);
        try {
            img = Image.createImage("/Image/ftt.jpg");
        } catch (IOException ex) {
            ex.getMessage();
        }
        append(img);
        append(tflogin);

        append(tfpassword);

        addCommand(cmd_cnx);
        addCommand(cmd_inscri);
        addCommand(cmdExit);
        setCommandListener(this);
    }

    public void commandAction(Command c, Displayable d) {
        if (c == cmd_cnx) {
            Thread th = new Thread(this);
            th.start();
        }
        if (c == cmd_inscri) {
            Midlet.mid.disp.setCurrent(new Inscription(""));
        }
        if (c == cmdExit) {
            Midlet.mid.notifyDestroyed();
        }

    }

    public void run() {
        String strlogin = tflogin.getString();
        String strpassword = tfpassword.getString();
        Utilisateur[] utilisateurs;
        try {
            utilisateurs = new UtilisateurDao().Authentification(strlogin, strpassword);
            if (utilisateurs.length == 1) {
                if (utilisateurs[0].getStatut() == 2) {
                    if (utilisateurs[0].getType().equals("admin")) {
                        a = new Alert("Information", "connexion avec succes admin", null, AlertType.INFO);
                        Midlet.mid.disp.setCurrent(new AdminMenu());
                    } else if (utilisateurs[0].getType().equals("arbitre")) {
                        a = new Alert("Information", "connexion avec succes arbitre", null, AlertType.INFO);
                        Midlet.mid.disp.setCurrent(new ArbitreMenu());
                    } else if (utilisateurs[0].getType().equals("joueur")) {
                        a = new Alert("Information", "connexion avec succes joueur", null, AlertType.INFO);
                        Midlet.mid.disp.setCurrent(new JoueurMenu());
                    } else if (utilisateurs[0].getType().equals("utilisateur")) {
                        a = new Alert("Information", "connexion avec succes utilisateur", null, AlertType.INFO);
                        Midlet.mid.disp.setCurrent(new UtilisateurMenu());
                    } else {
                        a = new Alert("Warning", "connexion refusee", null, AlertType.ALARM);

                    }

                }

                if (utilisateurs[0].getStatut() == 0) {
                    a = new Alert("Warning", "vous etes bannis", null, AlertType.ALARM);

                }

                if (utilisateurs[0].getStatut() == 1) {
                    a = new Alert("Warning", "votre compte n'est pas encore active", null, AlertType.ALARM);

                }

                Midlet.mid.disp.setCurrent(a);
            }

        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (SAXException ex) {
            a = new Alert("Warning", "connexion impossible", null, AlertType.ALARM);
            Midlet.mid.disp.setCurrent(a);
        } catch (ParserConfigurationException ex) {
            a = new Alert("Warning", "connexion impossible", null, AlertType.ALARM);
            Midlet.mid.disp.setCurrent(a);
        }

    }

}
