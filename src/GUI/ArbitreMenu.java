/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import java.io.IOException;
import javax.microedition.lcdui.Canvas;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.Image;
import pidevmob.Midlet;

/**
 *
 * @author ferid
 */
public class ArbitreMenu extends Canvas implements CommandListener {

    int position = 1;
    Image img;
Command cmd_deco = new Command("Deconnexion", Command.SCREEN, 0);
    protected void paint(Graphics g) {
        int w = getWidth();
        int h = getHeight();
        g.fillRect(0, 0, w, h);
        try {
            img = Image.createImage("/Image/menuarbitre.png");
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        addCommand(cmd_deco);
        setCommandListener(this);
        g.drawImage(img, 0, 10, 0);
        g.setColor(255, 0, 0);
        g.drawString("Matches", 25, 70, 0);
        g.drawString("News", 100, 70, 0);
        g.drawString("Calendrier", 165, 70, 0);
        g.drawString("formation", 20, 140, 0);
        g.drawString("Mise a niveau", 80, 140, 0);
        switch (position) {
            case 1:
                g.drawRect(25, 25, 40, 40);
                break;
            case 2:
                g.drawRect(100, 25, 40, 40);
                break;
            case 3:
                g.drawRect(175, 25, 40, 40);
                break;
            case 4:
                g.drawRect(25, 95, 40, 40);
                break;
            case 5:
                g.drawRect(100, 95, 40, 40);
                break;
        }
    }

    protected void keyPressed(int keyCode) {

        int gameAction = getGameAction(keyCode);

        if (gameAction == LEFT && position > 1) {
            position--;
        } else if (gameAction == RIGHT && position < 5) {
            position++;
        } else if (gameAction == UP && position > 3) {
            position -= 3;
        } else if (gameAction == DOWN && position < 3) {
            position += 3;
        } else if (gameAction == DOWN && position == 3) {
            position += 2;
        }
        if (gameAction == FIRE) {
            switch (position) {
                case 1:
                    Midlet.mid.disp.setCurrent(new ListedesMatchs(2));
                    break;
                case 2:
                    Midlet.mid.disp.setCurrent(new ListedesNews(2));
                    break;
                case 3:
                    Midlet.mid.disp.setCurrent(new PropreCalendrier());
                    break;
                case 4:
                    Midlet.mid.disp.setCurrent(new ListeDesFormation());
                    break;
                case 5:
                    Midlet.mid.disp.setCurrent(new ListeDesMiseANiveau());
                    break;

            }

        }
        repaint();
    }

    public void commandAction(Command c, Displayable d) {
        if (c==cmd_deco){
            Midlet.mid.disp.setCurrent(new Authentification(""));
        }
    }

}
