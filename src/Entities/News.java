/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import java.util.Date;

/**
 *
 * @author Raed
 */
public class News {

    private int id;
    private String titre;
    private String contenu;
    private String image;
    private String categorie;
    private String mot_clee;
    private String nomAuteur;
    private String contenu_min;
    private Date date;

    public News(int id, String titre, String contenu, String image, String categorie, String mot_clee, Date date) {
        this.id = id;
        this.titre = titre;
        this.contenu = contenu;
        this.image = image;
        this.categorie = categorie;
        this.mot_clee = mot_clee;
        this.date = date;
    }

    public News() {
    }

    public String toString() {
        return "News{" + "id=" + id + ", titre=" + titre + ", contenu=" + contenu + ", image=" + image + ", categorie=" + categorie + ", mot_clee=" + mot_clee + ", nomAuteur=" + nomAuteur + ", contenu_min=" + contenu_min + ", date=" + date + '}';
    }

    

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getContenu() {
        return contenu;
    }

    public void setContenu(String contenu) {
        this.contenu = contenu;
        if (contenu.length() >= 100) {
            this.contenu_min = contenu.substring(0, 100) + "...";
        } else {
            this.contenu_min = contenu;
        }
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getCategorie() {
        return categorie;
    }

    public void setCategorie(String categorie) {
        this.categorie = categorie;
    }

    public String getMot_clee() {
        return mot_clee;
    }

    public void setMot_clee(String mot_clee) {
        this.mot_clee = mot_clee;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getNomAuteur() {
        return nomAuteur;
    }

    public void setNomAuteur(String nomAuteur) {
        this.nomAuteur = nomAuteur;
    }
    
    public String getContenu_min() {
        return contenu_min;
    }

    public void setContenu_min(String contenu_min) {
        this.contenu_min = contenu_min;
    }

}
