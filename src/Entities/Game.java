/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

/**
 *
 * @author Salamandar
 */
public class Game {
    private int idM;
    private String arbitre;
    private String Terrain;
   
    private String date;

    public Game(int idM, String arbitre, String idTerain, String date) {
        this.idM = idM;
        this.arbitre = arbitre;
        this.Terrain = idTerain;
        this.date = date;
    }

    public Game() {
    }

    

    public int getIdM() {
        return idM;
    }

    public void setIdM(int idM) {
        this.idM = idM;
    }

    public String getArbitre() {
        return arbitre;
    }

    public void setArbitre(String arbitre) {
        this.arbitre = arbitre;
    }

    public String getTerain() {
        return Terrain;
    }

    public void setTerain(String Terain) {
        this.Terrain = Terain;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String toString() {
        return "Match{" + "idM=" + idM + ", arbitre=" + arbitre + ", idTerain=" + Terrain + ", date=" + date + '}';
    }

    
    
    
}
