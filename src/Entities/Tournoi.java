/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

/**
 *
 * @author ferid
 */
public class Tournoi {
    int id_event;
    String nom;
    String emplacement;
    String date_ouverture;
    String date_cloture;
    String type_event;
    String type_tournoi;
    String logo;

    public Tournoi() {
    }

    public Tournoi(int id_event, String nom, String emplacement, String date_ouverture, String date_cloture, String type_event, String type_tournoi, String logo) {
        this.id_event = id_event;
        this.nom = nom;
        this.emplacement = emplacement;
        this.date_ouverture = date_ouverture;
        this.date_cloture = date_cloture;
        this.type_event = type_event;
        this.type_tournoi = type_tournoi;
        this.logo = logo;
    }

    public int getId_event() {
        return id_event;
    }

    public void setId_event(int id_event) {
        this.id_event = id_event;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getEmplacement() {
        return emplacement;
    }

    public void setEmplacement(String emplacement) {
        this.emplacement = emplacement;
    }

    public String getDate_ouverture() {
        return date_ouverture;
    }

    public void setDate_ouverture(String date_ouverture) {
        this.date_ouverture = date_ouverture;
    }

    public String getDate_cloture() {
        return date_cloture;
    }

    public void setDate_cloture(String date_cloture) {
        this.date_cloture = date_cloture;
    }

    public String getType_event() {
        return type_event;
    }

    public void setType_event(String type_event) {
        this.type_event = type_event;
    }

    public String getType_tournoi() {
        return type_tournoi;
    }

    public void setType_tournoi(String type_tournoi) {
        this.type_tournoi = type_tournoi;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

   
    
}
