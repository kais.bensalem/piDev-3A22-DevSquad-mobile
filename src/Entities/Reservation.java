/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

/**
 *
 * @author Raed
 */
public class Reservation {

    private int id;
    private Utilisateur u;
    private Match m;
    private boolean achat;
    private int cat1;
    private int cat2;
    private int cat3;
    private int cat4;
    private int prix;

    public Reservation(int id, Utilisateur u, Match m, boolean achat, int cat1, int cat2, int cat3, int cat4, int prix) {
        this.id = id;
        this.u = u;
        this.m = m;
        this.achat = achat;
        this.cat1 = cat1;
        this.cat2 = cat2;
        this.cat3 = cat3;
        this.cat4 = cat4;
        this.prix = prix;
    }

    public Reservation() {
    }

    
    public String toString() {
        return "Reservation{" + "id=" + id + ", u=" + u + ", m=" + m + ", achat=" + achat + ", cat1=" + cat1 + ", cat2=" + cat2 + ", cat3=" + cat3 + ", cat4=" + cat4 + ", prix=" + prix + '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Utilisateur getU() {
        return u;
    }

    public void setU(Utilisateur u) {
        this.u = u;
    }

    public Match getM() {
        return m;
    }

    public void setM(Match m) {
        this.m = m;
    }

    public boolean isAchat() {
        return achat;
    }

    public void setAchat(boolean achat) {
        this.achat = achat;
    }

    public int getCat1() {
        return cat1;
    }

    public void setCat1(int cat1) {
        this.cat1 = cat1;
    }

    public int getCat2() {
        return cat2;
    }

    public void setCat2(int cat2) {
        this.cat2 = cat2;
    }

    public int getCat3() {
        return cat3;
    }

    public void setCat3(int cat3) {
        this.cat3 = cat3;
    }

    public int getCat4() {
        return cat4;
    }

    public void setCat4(int cat4) {
        this.cat4 = cat4;
    }

    public int getPrix() {
        return prix;
    }

    public void setPrix(int prix) {
        this.prix = prix;
    }

}
