/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

/**
 *
 * @author ferid
 */
public class Match {
    int id_match;
    String nom;
    String prenom;
    String date;
    String type;
    int id_tournoi;

    public Match() {
    }

    public Match(int id_match, String nom, String prenom, String date, String type, int id_tournoi) {
        this.id_match = id_match;
        this.nom = nom;
        this.prenom = prenom;
        this.date = date;
        this.type = type;
        this.id_tournoi = id_tournoi;
    }

    public int getId_match() {
        return id_match;
    }

    public void setId_match(int id_match) {
        this.id_match = id_match;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getId_terrain() {
        return id_tournoi;
    }

    public void setId_terrain(int id_terrain) {
        this.id_tournoi = id_terrain;
    }
    
}
