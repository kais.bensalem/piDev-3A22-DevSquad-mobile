/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;


/**
 *
 * @author Raed
 */
public class Pari {

    private int id;
    //private ArrayList<Prediction> predictions;
    private Utilisateur u;
    private int montant_payer;
    private int cote;
    private int montant_gagner;

    public Pari(int id, Utilisateur u, int montant_payer, int cote, int montant_gagner) {
        this.id = id;
        //this.predictions = predictions;
        this.u = u;
        this.montant_payer = montant_payer;
        this.cote = cote;
        this.montant_gagner = montant_gagner;
    }

    public Pari() {

    }

    public String toString() {
        return "Pari{" + "id=" + id + ", u=" + u + ", montant_payer=" + montant_payer + ", cote=" + cote + ", montant_gagner=" + montant_gagner + '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

//    public ArrayList<Prediction> getPredictions() {
//        return predictions;
//    }
//
//    public void setPredictions(ArrayList<Prediction> predictions) {
//        this.predictions = predictions;
//    }

    public Utilisateur getU() {
        return u;
    }

    public void setU(Utilisateur u) {
        this.u = u;
    }

    public int getMontant_payer() {
        return montant_payer;
    }

    public void setMontant_payer(int montant_payer) {
        this.montant_payer = montant_payer;
    }

    public int getCote() {
        return cote;
    }

    public void setCote(int cote) {
        this.cote = cote;
    }

    public int getMontant_gagner() {
        return montant_gagner;
    }

    public void setMontant_gagner(int montant_gagner) {
        this.montant_gagner = montant_gagner;
    }

}
