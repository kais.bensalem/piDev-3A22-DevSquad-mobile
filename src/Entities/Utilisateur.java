/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

/**
 *
 * @author ferid
 */
public class Utilisateur {
    protected int id;
    protected String nom;
   protected String prenom;
    protected String login;
    protected String password;
    protected String type;
    
    protected String cin;
    protected String adresse;
    protected String email;
    protected String date_naissance;
    protected String sexe;
    protected long tel;
    protected String nationalite;
    protected int statut;
    
    
    
    public Utilisateur () {}
    
    
    /*************avec id sans type *************/
    public Utilisateur(int id, String nom,String prenom, String login, String password) {
        this.id = id;
        this.nom = nom;
        this.prenom=prenom;
        this.login = login;
        this.password = password;
                        
    }
/********************* sans id sans type **********/
    public Utilisateur(String nom, String prenom, String login, String password) {
        this.nom = nom;
        this.prenom = prenom;
        this.login = login;
        this.password = password;
    }
    
     public Utilisateur(String nom, String prenom, String login, String password, String type, String cin, String adresse, String email, String date_naissance, String sexe, long tel, String nationalite) {
        this.nom = nom;
        this.prenom = prenom;
        this.login = login;
        this.password = password;
        this.type = type;
        this.cin = cin;
        this.adresse = adresse;
        this.email = email;
        this.date_naissance = date_naissance;
        this.sexe = sexe;
        this.tel = tel;
        this.nationalite = nationalite;
    }

    public Utilisateur(String nom, String prenom, String login, String password, String cin, String adresse, String email, String date_naissance, String sexe, long tel, String nationalite) {
        this.nom = nom;
        this.prenom = prenom;
        this.login = login;
        this.password = password;
        this.cin = cin;
        this.adresse = adresse;
        this.email = email;
        this.date_naissance = date_naissance;
        this.sexe = sexe;
        this.tel = tel;
        this.nationalite = nationalite;
        this.type="utilisateur";
        
    }
     
     
    
/**** avec type et id *************/
    public Utilisateur(int id, String nom, String prenom, String login, String password, String type) {
        this.id = id;
        this.nom = nom;
        this.prenom = prenom;
        this.login = login;
        this.password = password;
        this.type = type;
    }
/***************** avec type sans id ************/
    public Utilisateur(String nom, String prenom, String login, String password, String type) {
        this.nom = nom;
        this.prenom = prenom;
        this.login = login;
        this.password = password;
        this.type = type;
    }

    public Utilisateur(int id, String nom, String prenom, String adresse, String date_naissance, String sexe, String nationalite) {
        this.id = id;
        this.nom = nom;
        this.prenom = prenom;
        this.adresse = adresse;
        this.date_naissance = date_naissance;
        this.sexe = sexe;
        this.nationalite = nationalite;
    }


    

    
  
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
    
    

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getCin() {
        return cin;
    }

    public void setCin(String cin) {
        this.cin = cin;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDate_naissance() {
        return date_naissance;
    }

    public void setDate_naissance(String date_naissance) {
        this.date_naissance = date_naissance;
    }

    public String getSexe() {
        return sexe;
    }

    public void setSexe(String sexe) {
        this.sexe = sexe;
    }

    public long getTel() {
        return tel;
    }

    public void setTel(long tel) {
        this.tel = tel;
    }

    public String getNationalite() {
        return nationalite;
    }

    public void setNationalite(String nationalite) {
        this.nationalite = nationalite;
    }

    public int getStatut() {
        return statut;
    }

    public void setStatut(int statut) {
        this.statut = statut;
    }

    public String toString() {
        return "Utilisateur{" + "nom=" + nom + ", prenom=" + prenom + ", login=" + login + ", password=" + password + ", type=" + type + ", cin=" + cin + ", adresse=" + adresse + ", email=" + email + ", date_naissance=" + date_naissance + ", sexe=" + sexe + ", tel=" + tel + ", nationalite=" + nationalite + ", statut=" + statut + '}';
    }
    
    
    
}
