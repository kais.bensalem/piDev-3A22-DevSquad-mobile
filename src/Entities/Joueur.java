/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

/**
 *
 * @author ferid
 */
public class Joueur extends Utilisateur {
      long rang;
    String statut_joueur;
String photo;
    public Joueur() {
    }

    
    public Joueur(int id, String nom, String prenom, String adresse, String date_naissance, String sexe, String nationalite,long rang,String statut_joueur) {
        super(id, nom, prenom, adresse, date_naissance, sexe, nationalite);
        this.rang=rang;
        this.statut_joueur=statut_joueur;
    }

    public long getRang() {
        return rang;
    }

    public void setRang(long rang) {
        this.rang = rang;
    }

    public String getStatut_joueur() {
        return statut_joueur;
    }

    public void setStatut_joueur(String statut_joueur) {
        this.statut_joueur = statut_joueur;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }
    
    
    
    
}
