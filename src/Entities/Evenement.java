/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

/**
 *
 * @author Salamandar
 */
public class Evenement {
     private int id;
    private String nom;
    private String emplacement;
    private String date_ouverture;
    private String date_cloture;
    private int nb_participant;
    private String type_event;

    public Evenement(int id, String nom, String emplacement, String date_ouverture, String date_cloture, int nb_participant) {
        this.id = id;
        this.nom = nom;
        this.emplacement = emplacement;
        this.date_ouverture = date_ouverture;
        this.date_cloture = date_cloture;
        this.nb_participant = nb_participant;
    }

    public Evenement(int id, String nom, String emplacement, String date_ouverture, String date_cloture, int nb_participant, String type_event) {
        this.id = id;
        this.nom = nom;
        this.emplacement = emplacement;
        this.date_ouverture = date_ouverture;
        this.date_cloture = date_cloture;
        this.nb_participant = nb_participant;
        this.type_event = type_event;
    }

    public Evenement() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getEmplacement() {
        return emplacement;
    }

    public void setEmplacement(String emplacement) {
        this.emplacement = emplacement;
    }

    public String getDate_ouverture() {
        return date_ouverture;
    }

    public void setDate_ouverture(String date_ouverture) {
        this.date_ouverture = date_ouverture;
    }

    public String getDate_cloture() {
        return date_cloture;
    }

    public void setDate_cloture(String date_cloture) {
        this.date_cloture = date_cloture;
    }

    public int getNb_participant() {
        return nb_participant;
    }

    public void setNb_participant(int nb_participant) {
        this.nb_participant = nb_participant;
    }

    public String getType_event() {
        return type_event;
    }

    public void setType_event(String type_event) {
        this.type_event = type_event;
    }

    public String toString() {
        return "Evenement{" + "id=" + id + ", nom=" + nom + ", emplacement=" + emplacement + ", date_ouverture=" + date_ouverture + ", date_cloture=" + date_cloture + ", nb_participant=" + nb_participant + ", type_event=" + type_event + '}';
    }

}
