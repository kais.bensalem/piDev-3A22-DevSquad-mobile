/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Entities.Joueur;
import Entities.Match;
import Entities.Tournoi;
import Handler.MatchHandler;
import Handler.TournoiHandler;
import java.io.DataInputStream;
import java.io.IOException;
import javax.microedition.io.Connector;
import javax.microedition.io.HttpConnection;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.SAXException;

/**
 *
 * @author ferid
 */
public class TournoiDao {
    Tournoi  [] Tournois;
    
    
      public Tournoi[] select() throws IOException, SAXException, ParserConfigurationException{
         TournoiHandler tournoihandler = new TournoiHandler();
          SAXParser SAXparser = SAXParserFactory.newInstance().newSAXParser();
          DataInputStream dis;
          HttpConnection hc = null;
          try {
            
            // get a parser object
           
            // get an InputStream from somewhere (could be HttpConnection, for example)
         hc = (HttpConnection)Connector.open("http://localhost/MobilePidev/listedestournoi.php"); 
           
        }  catch (IOException ex) {
            ex.printStackTrace();
        }
       dis = new DataInputStream(hc.openDataInputStream());
            SAXparser.parse(dis, tournoihandler);
            // display the result
            Tournois = tournoihandler.getTournoi();
           
             return Tournois;
             
      
   }
       public Tournoi[] selectparjoueur(Joueur j) throws IOException, SAXException, ParserConfigurationException{
          TournoiHandler tournoihandler = new TournoiHandler();
          
          SAXParser SAXparser = SAXParserFactory.newInstance().newSAXParser();
          DataInputStream dis;
          HttpConnection hc = null;
          try {
          
             
            // get a parser object
           
            // get an InputStream from somewhere (could be HttpConnection, for example)
         hc = (HttpConnection)Connector.open("http://localhost/MobilePidev/listedesmatchsselonJoueur.php?id_joueur="+j.getId()); 
           
        }  catch (IOException ex) {
            ex.printStackTrace();
        }
       dis = new DataInputStream(hc.openDataInputStream());
            SAXparser.parse(dis, tournoihandler);
            // display the result
            Tournois = tournoihandler.getTournoi();
            System.out.println(Tournois);
             return Tournois;
             
      
   }
      
      
        
}
