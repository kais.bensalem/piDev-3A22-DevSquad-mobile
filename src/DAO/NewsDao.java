/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Entities.News;
import Handler.NewsHandler;
import java.io.DataInputStream;
import java.io.IOException;
import javax.microedition.io.Connector;
import javax.microedition.io.HttpConnection;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.SAXException;

/**
 *
 * @author Raed
 */
public class NewsDao {

    News[] news;

    public News[] select() throws IOException, SAXException, ParserConfigurationException {
        NewsHandler newsHandler = new NewsHandler();
        SAXParser SAXparser = SAXParserFactory.newInstance().newSAXParser();
        DataInputStream dis;
        HttpConnection hc = null;
        try {
            hc = (HttpConnection) Connector.open("http://localhost/MobilePidev/news.php?action=select");

        } catch (IOException ex) {
            ex.printStackTrace();
        }
        dis = new DataInputStream(hc.openDataInputStream());
        SAXparser.parse(dis, newsHandler);
        news = newsHandler.getNews();
        return news;
    }
    public News[] getByCategorie(String categorie) throws IOException, SAXException, ParserConfigurationException{
        NewsHandler newsHandler = new NewsHandler();
        SAXParser SAXparser = SAXParserFactory.newInstance().newSAXParser();
        DataInputStream dis;
        HttpConnection hc = null;
        try {
            hc = (HttpConnection) Connector.open("http://localhost/MobilePidev/news.php?action=search&categorie=" + categorie);

        } catch (IOException ex) {
            ex.printStackTrace();
        }
        dis = new DataInputStream(hc.openDataInputStream());
        SAXparser.parse(dis, newsHandler);
        news = newsHandler.getNews();
        return news;
    }
}
