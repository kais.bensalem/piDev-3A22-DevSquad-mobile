/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Entities.News;
import Entities.Reservation;
import Handler.NewsHandler;
import Handler.ReservationHandler;
import java.io.DataInputStream;
import java.io.IOException;
import javax.microedition.io.Connector;
import javax.microedition.io.HttpConnection;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.SAXException;

/**
 *
 * @author Raed
 */
public class ReservationDao {
Reservation[] reservations;
    public void ajouter(Reservation r) throws IOException {
        HttpConnection hc = null;
        try {
            hc = (HttpConnection) Connector.open("http://localhost/MobilePidev/ajouter_reservation?match="+ r.getM().getId_match()+"&user=" + r.getU().getId() );

        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
    public Reservation[] select() throws IOException, SAXException, ParserConfigurationException {
        ReservationHandler reservationHandler = new ReservationHandler();
        SAXParser SAXparser = SAXParserFactory.newInstance().newSAXParser();
        DataInputStream dis;
        HttpConnection hc = null;
        try {
            hc = (HttpConnection) Connector.open("http://localhost/MobilePidev/reservations.php?action=select");

        } catch (IOException ex) {
            ex.printStackTrace();
        }
        dis = new DataInputStream(hc.openDataInputStream());
        SAXparser.parse(dis, reservationHandler);
        reservations = reservationHandler.getReservations();
        return reservations;
    }
}
