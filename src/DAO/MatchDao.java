/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Entities.Joueur;
import Entities.Match;
import Handler.MatchHandler;
import java.io.DataInputStream;
import java.io.IOException;
import javax.microedition.io.Connector;
import javax.microedition.io.HttpConnection;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.SAXException;

/**
 *
 * @author ferid
 */
public class MatchDao {
    Match  [] matchs;
    
    
      public Match[] select() throws IOException, SAXException, ParserConfigurationException{
          MatchHandler matchHandler = new MatchHandler();
          SAXParser SAXparser = SAXParserFactory.newInstance().newSAXParser();
          DataInputStream dis;
          HttpConnection hc = null;
          try {
            
            // get a parser object
           
            // get an InputStream from somewhere (could be HttpConnection, for example)
         hc = (HttpConnection)Connector.open("http://localhost/MobilePidev/listedesmatchs.php"); 
           
        }  catch (IOException ex) {
            ex.printStackTrace();
        }
       dis = new DataInputStream(hc.openDataInputStream());
            SAXparser.parse(dis, matchHandler);
            // display the result
            matchs = matchHandler.getMatch();
             return matchs;
      
   }
       public Match[] selectparjoueur(Joueur j) throws IOException, SAXException, ParserConfigurationException{
          MatchHandler matchHandler = new MatchHandler();
          
          SAXParser SAXparser = SAXParserFactory.newInstance().newSAXParser();
          DataInputStream dis;
          HttpConnection hc = null;
          try {
          
             
            // get a parser object
           
            // get an InputStream from somewhere (could be HttpConnection, for example)
         hc = (HttpConnection)Connector.open("http://localhost/MobilePidev/listedesmatchsselonJoueur.php?id_joueur="+j.getId()); 
           
        }  catch (IOException ex) {
            ex.printStackTrace();
        }
       dis = new DataInputStream(hc.openDataInputStream());
            SAXparser.parse(dis, matchHandler);
            // display the result
            matchs = matchHandler.getMatch();
            System.out.println(matchs);
             return matchs;
             
      
   }
       public Match[] selectpartournoi(int id) throws IOException, SAXException, ParserConfigurationException{
          MatchHandler matchHandler = new MatchHandler();
          
          SAXParser SAXparser = SAXParserFactory.newInstance().newSAXParser();
          DataInputStream dis;
          HttpConnection hc = null;
          try {
          
             
            // get a parser object
           
            // get an InputStream from somewhere (could be HttpConnection, for example)
         hc = (HttpConnection)Connector.open("http://localhost/MobilePidev/listedesmatchsselonTournoi.php?id_tournoi=39"); 
           
        }  catch (IOException ex) {
            ex.printStackTrace();
        }
       dis = new DataInputStream(hc.openDataInputStream());
            SAXparser.parse(dis, matchHandler);
            // display the result
            matchs = matchHandler.getMatch();
            System.out.println(matchs);
             return matchs;
             
      
   }
      
      
        
}
