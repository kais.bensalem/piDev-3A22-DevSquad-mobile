/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Entities.Medecin;
import Handler.MedecinHandler;
import java.io.DataInputStream;
import java.io.IOException;
import javax.microedition.io.Connector;
import javax.microedition.io.HttpConnection;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.SAXException;

/**
 *
 * @author ferid
 */
public class MedecinDao {
    Medecin  [] medecins;
    
    
      public Medecin[] select() throws IOException, SAXException, ParserConfigurationException{
          MedecinHandler medecinHandler = new MedecinHandler();
          SAXParser SAXparser = SAXParserFactory.newInstance().newSAXParser();
          DataInputStream dis;
          HttpConnection hc = null;
          try {
            
            // get a parser object
           
            // get an InputStream from somewhere (could be HttpConnection, for example)
           hc = (HttpConnection)Connector.open("http://localhost/MobilePidev/listedesmedecins.php"); 
         
        }  catch (IOException ex) {
            ex.printStackTrace();
        }
       dis = new DataInputStream(hc.openDataInputStream());
            SAXparser.parse(dis, medecinHandler);
            // display the result
            medecins = medecinHandler.getMedecin();
             return medecins;
      
   }
      
      
         public boolean insert(Medecin u){
        try {
            HttpConnection hc = (HttpConnection)Connector.open("http://localhost/MobilePidev/inscri.php?nom="+u.getNom()+"&prenom="+u.getPrenom()+"&login="+u.getLogin()+"&password="+u.getPassword()+"&type="+u.getType()+"&cin="+u.getCin()+"&adresse="+u.getAdresse()+"&email="+u.getEmail()+"&date="+u.getDate_naissance()+"&sexe="+u.getSexe()+"&tel="+u.getTel()+"&nationalite="+u.getNationalite()); 
            DataInputStream dis = new DataInputStream(hc.openDataInputStream());
           StringBuffer sb = new StringBuffer();
           int ch;
            while ((ch = dis.read())!=-1) {
                sb.append((char)ch);                
            }
            if (sb.toString().trim().equals("success")) {
                return true;
            }
            
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return false;
    }
}
