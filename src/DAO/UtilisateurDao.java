/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Entities.Utilisateur;
import Handler.UtilisateurHandler;
import java.io.DataInputStream;
import java.io.IOException;
import javax.microedition.io.Connector;
import javax.microedition.io.HttpConnection;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.SAXException;

/**
 *
 * @author ferid
 */
public class UtilisateurDao {
    Utilisateur  [] utilisateurs;
    
    
      public Utilisateur[] Authentification(String login,String password) throws IOException, SAXException, ParserConfigurationException{
          UtilisateurHandler utilisateurHandler = new UtilisateurHandler();
          SAXParser SAXparser = SAXParserFactory.newInstance().newSAXParser();
          DataInputStream dis;
          HttpConnection hc = null;
          try {
            
            // get a parser object
           
            // get an InputStream from somewhere (could be HttpConnection, for example)
            hc = (HttpConnection) Connector.open("http://localhost/MobilePidev/authentification.php?login="+login+"&password="+password);//people.xml est un exemple
     
        }  catch (IOException ex) {
            ex.printStackTrace();
        }
       dis = new DataInputStream(hc.openDataInputStream());
            SAXparser.parse(dis, utilisateurHandler);
            // display the result
            utilisateurs = utilisateurHandler.getUtilisateur();
             return utilisateurs;
      
   }
      
      
         public boolean insert(Utilisateur u){
        try {
            HttpConnection hc = (HttpConnection)Connector.open("http://localhost/MobilePidev/inscri.php?nom="+u.getNom()+"&prenom="+u.getPrenom()+"&login="+u.getLogin()+"&password="+u.getPassword()+"&type="+u.getType()+"&cin="+u.getCin()+"&adresse="+u.getAdresse()+"&email="+u.getEmail()+"&date="+u.getDate_naissance()+"&sexe="+u.getSexe()+"&tel="+u.getTel()+"&nationalite="+u.getNationalite()); 
            DataInputStream dis = new DataInputStream(hc.openDataInputStream());
           StringBuffer sb = new StringBuffer();
           int ch;
            while ((ch = dis.read())!=-1) {
                sb.append((char)ch);                
            }
            if (sb.toString().trim().equals("success")) {
                return true;
            }
            
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return false;
    }
         
           public Utilisateur[] nouveaux_inscri() throws IOException, SAXException, ParserConfigurationException{
          UtilisateurHandler utilisateurHandler = new UtilisateurHandler();
          SAXParser SAXparser = SAXParserFactory.newInstance().newSAXParser();
          DataInputStream dis;
          HttpConnection hc = null;
          try {
            
            // get a parser object
           
            // get an InputStream from somewhere (could be HttpConnection, for example)
            hc = (HttpConnection) Connector.open("http://localhost/MobilePidev/nouveauxinscri.php");//people.xml est un exemple
     
        }  catch (IOException ex) {
            ex.printStackTrace();
            
        }
       dis = new DataInputStream(hc.openDataInputStream());
            SAXparser.parse(dis, utilisateurHandler);
            // display the result
            utilisateurs = utilisateurHandler.getUtilisateur();
             return utilisateurs;
      
   }
           
                  public static boolean valider(){
        try {
            HttpConnection hc = (HttpConnection)Connector.open("http://localhost/MobilePidev/validerInscri.php"); 
            DataInputStream dis = new DataInputStream(hc.openDataInputStream());
           StringBuffer sb = new StringBuffer();
           int ch;
            while ((ch = dis.read())!=-1) {
                sb.append((char)ch);                
            }
            if (sb.toString().trim().equals("success")) {
                return true;
            }
            
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return false;
    }
                
      public Utilisateur[] ChercherUtilisateur(String login) throws IOException, SAXException, ParserConfigurationException{
          UtilisateurHandler utilisateurHandler = new UtilisateurHandler();
          SAXParser SAXparser = SAXParserFactory.newInstance().newSAXParser();
          DataInputStream dis;
          HttpConnection hc = null;
          try {
            
            // get a parser object
           
            // get an InputStream from somewhere (could be HttpConnection, for example)
            hc = (HttpConnection)Connector.open("http://localhost/MobilePidev/ChercherUtilisateur.php?login="+login); 
//people.xml est un exemple
     
        }  catch (IOException ex) {
            ex.printStackTrace();
        }
       dis = new DataInputStream(hc.openDataInputStream());
            SAXparser.parse(dis, utilisateurHandler);
            // display the result
            utilisateurs = utilisateurHandler.getUtilisateur();
             return utilisateurs;
      
   }      
                public boolean supprimer(String login){
        try {
            HttpConnection hc = (HttpConnection)Connector.open("http://localhost/MobilePidev/SupprimerUtilisateur.php?login="+login); 
            DataInputStream dis = new DataInputStream(hc.openDataInputStream());
           StringBuffer sb = new StringBuffer();
           int ch;
            while ((ch = dis.read())!=-1) {
                sb.append((char)ch);                
            }
            if (sb.toString().trim().equals("success")) {
                return true;
            }
            
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return false;
    }
                
    public  boolean bannir(String login){
        try {
            HttpConnection hc = (HttpConnection)Connector.open("http://localhost/MobilePidev/Bannir.php?login="+login); 
            DataInputStream dis = new DataInputStream(hc.openDataInputStream());
           StringBuffer sb = new StringBuffer();
           int ch;
            while ((ch = dis.read())!=-1) {
                sb.append((char)ch);                
            }
            if (sb.toString().trim().equals("success")) {
                return true;
            }
            
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return false;
    }
           
}
