/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Entities.Joueur;
import Handler.JoueurHandler;
import java.io.DataInputStream;
import java.io.IOException;
import javax.microedition.io.Connector;
import javax.microedition.io.HttpConnection;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.SAXException;

/**
 *
 * @author ferid
 */
public class JoueurDao {
    Joueur  [] joueurs;
    
    
      public Joueur[] select() throws IOException, SAXException, ParserConfigurationException{
          JoueurHandler joueurHandler = new JoueurHandler();
          SAXParser SAXparser = SAXParserFactory.newInstance().newSAXParser();
          DataInputStream dis;
          HttpConnection hc = null;
          try {
            
            // get a parser object
           
            // get an InputStream from somewhere (could be HttpConnection, for example)
         hc = (HttpConnection)Connector.open("http://localhost/MobilePidev/listedesjoueurs.php"); 
           
        }  catch (IOException ex) {
            ex.printStackTrace();
        }
       dis = new DataInputStream(hc.openDataInputStream());
            SAXparser.parse(dis, joueurHandler);
            // display the result
            joueurs = joueurHandler.getJoueur();
             return joueurs;
      
   }
      
      
         public boolean insert(Joueur u){
        try {
            HttpConnection hc = (HttpConnection)Connector.open("http://localhost/MobilePidev/listedesjoueurs.php"); 
            DataInputStream dis = new DataInputStream(hc.openDataInputStream());
           StringBuffer sb = new StringBuffer();
           int ch;
            while ((ch = dis.read())!=-1) {
                sb.append((char)ch);                
            }
            if (sb.toString().trim().equals("success")) {
                return true;
            }
            
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return false;
    }
}
